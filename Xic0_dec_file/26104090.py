# file /home/xuao/cmtuser/GaussDev_v49r11/Gen/DecFiles/options/26104090.py generated: Mon, 07 Jan 2019 09:44:03
#
# Event Type: 26104090
#
# ASCII decay Descriptor: {[Xi_c0 -> p+ K- K- pi+]cc}
#
from Configurables import Generation
Generation().EventType = 26104090
Generation().SampleGenerationTool = "SignalPlain"
from Configurables import SignalPlain
Generation().addTool( SignalPlain )
Generation().SignalPlain.ProductionTool = "PythiaProduction"
from Configurables import ToolSvc
from Configurables import EvtGenDecay
ToolSvc().addTool( EvtGenDecay )
ToolSvc().EvtGenDecay.UserDecayFile = "$DECFILESROOT/dkfiles/Xic0_pKKpi=phsp,DecProdCut,TightCut,tau=250fs.dec"
Generation().SignalPlain.CutTool = "LoKi::GenCutTool/TightCut"
Generation().SignalPlain.SignalPIDList = [ 4232,-4232 ]
from Configurables import LHCb__ParticlePropertySvc
LHCb__ParticlePropertySvc().Particles = [ "Xi_c0               106        4132  0.0        2.47100000      2.5e-13            Xi_c0        4132   0.000", "Xi_c~0              107       -4132  0.0        2.47100000      2.5e-13          anti-Xi_c0       -4132   0.000" ]


from Configurables import LoKi__GenCutTool
from Gauss.Configuration import *
generation = Generation()
signal     = generation.SignalPlain
signal.addTool ( LoKi__GenCutTool , 'TightCut' )
tightCut   = signal.TightCut
tightCut.Decay     = '^[Xi_c0 ==> ^p+ ^K- ^K- ^pi+]CC'
tightCut.Preambulo += [
     'from GaudiKernel.SystemOfUnits import MeV'
    ,'inAcc        =  in_range ( 0.010 , GTHETA , 0.400 ) '
    ,'fastTrack    =  ( GPT > 400 * MeV ) & ( GP  > 800 * MeV ) '
]
tightCut.Cuts     =    {
     '[p+]cc'     : 'inAcc & fastTrack'
    ,'[K-]cc'     : 'inAcc & fastTrack'
    ,'[pi+]cc'    : 'inAcc & fastTrack'
    }


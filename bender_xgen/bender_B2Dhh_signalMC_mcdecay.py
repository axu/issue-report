#!/usr/bin/env python

"""
Bender module to run the following sequence over B2Dhh signal MC samples:
- run an algorithm to store the MC truth DP position (and other info) for all generated events
- run an algorithm to store the reco and MC truth-matched info for all Stripped candidates
"""

import sys
from Bender.MainMC import *

def configure( datafiles, catalogues = [], params = {}, castor = False ) :

    #=============== Configuration and Setup ===============#
    btype     = params.get( 'btype',      'Bd'            )
    track1    = params.get( 'track1',     'pi'            )
    track2    = params.get( 'track2',     'pi'            )
    ctype     = params.get( 'ctype',      'D0'            )
    gdaug1    = params.get( 'gdaug1',     'K'             )
    gdaug2    = params.get( 'gdaug2',     'K'             )
    magType   = params.get( 'magType',    'MagUp'         )
    stripping = params.get( 'stripping',  'Stripping28r1' )
    stream    = params.get( 'stream',     'AllStreams'    )
    inputType = params.get( 'inputType',  'DST'           )
    dddbtag   = params.get( 'dddbtag',    ''              )
    conddbtag = params.get( 'conddbtag',  ''              )
    printFreq = params.get( 'printFreq',  1000            )
    simType   = params.get( 'simType',    'Sim09c'        )
    #=======================================================#

    print(params)

    knownMagnetTypes = [ 'MagDown', 'MagUp' ]
    if magType not in knownMagnetTypes :
        e = Exception('Unsupported magnet setting: ' + magType)
        raise e

    known2011StrippingVersions = [ 'Stripping21r1' , 'Stripping21r1p2' ]
    known2012StrippingVersions = [ 'Stripping21'   , 'Stripping21r0p2' ]
    known2015StrippingVersions = [ 'Stripping24r1' ]
    known2016StrippingVersions = [ 'Stripping28r1' ]
    known2017StrippingVersions = [ 'Stripping29r2' ]
    known2018StrippingVersions = [ 'Stripping34'   ]

    dataType = ''
    if stripping in known2011StrippingVersions:
        dataType = '2011'
    elif stripping in known2012StrippingVersions:
        dataType = '2012'
    elif stripping in known2015StrippingVersions:
        dataType = '2015'
    elif stripping in known2016StrippingVersions:
        dataType = '2016'
    elif stripping in known2017StrippingVersions:
        dataType = '2017'
    elif stripping in known2018StrippingVersions:
        dataType = '2018'
    else :
        e = Exception('Unsupported Stripping version: ' + stripping)
        raise e

    knownStreams = {}
    knownStreams['ALLSTREAMS']      = 'AllStreams'
    knownStreams['B2DHH.STRIP']     = 'b2dhh.Strip'
    knownStreams['B02D0PIPI.STRIP'] = 'B02D0pipi.Strip'
    if stream.upper() not in knownStreams :
        e = Exception('Unknown input stream name: ' + stream)
        raise e
    stream = knownStreams[ stream.upper() ]

    knownInputTypes = [ 'DST', 'MDST', 'XGEN' ]
    if inputType not in knownInputTypes :
        e = Exception('Unknown input file type: ' + inputType)
        raise e
    dvInputType = inputType
    if inputType == 'XGEN' :
        dvInputType = 'DST'

    mode = btype+'2'+track1+track2+ctype
    mc_daughters = [ track1, track2, ctype ]
    mc_gdaughters = [ gdaug1, gdaug2 ]

    mc_decay_descriptors = []

    reco_daughters = [ 'pi', 'pi', 'D' ]

    algName = 'B2Dhh'

    teslocation = '/Event/' + stream

    inputLocation = 'Phys/B02D0PiPiD2HHBeauty2CharmLine/Particles'

    if dvInputType == 'DST' :
        inputLocation = teslocation + '/' + inputLocation


    # Configuration of DaVinci
    from Configurables import DaVinci

    daVinci = DaVinci()
    daVinci.DataType        = dataType
    daVinci.Simulation      = True
    daVinci.Lumi            = False
    daVinci.InputType       = dvInputType
    daVinci.TupleFile       = mode+'-'+ctype+'2'+gdaug1+gdaug2+'-MC-'+dataType+'-'+magType+'-'+stripping+'-'+simType+'-withMCtruth.root'
    daVinci.EvtMax          = -1
    daVinci.PrintFreq       = printFreq
    if inputType == 'MDST':
        daVinci.RootInTES   = teslocation

    ## try to get the tags from Rec/Header
    if dddbtag != '' and conddbtag != '' :
        daVinci.DDDBtag = dddbtag
        daVinci.CondDBtag = conddbtag
    else :
        from BenderTools.GetDBtags import getDBTags
        tags = getDBTags ( datafiles[0] , castor  )

        logger.info ( 'Extract tags from DATA : %s' % tags )
        if tags.has_key ( 'DDDB' ) and tags ['DDDB'] :
            daVinci.DDDBtag   = tags['DDDB'  ]
            logger.info ( 'Set DDDB    %s ' % daVinci.DDDBtag   )
        if tags.has_key ( 'CONDDB' ) and tags ['CONDDB'] :
            daVinci.CondDBtag = tags['CONDDB']
            logger.info ( 'Set CONDDB  %s ' % daVinci.CondDBtag )
        if tags.has_key ( 'SIMCOND' ) and tags ['SIMCOND'] :
            daVinci.CondDBtag = tags['SIMCOND']
            logger.info ( 'Set SIMCOND %s ' % daVinci.CondDBtag )

    if ('md' in daVinci.CondDBtag and magType == 'MagUp') or ('mu' in daVinci.CondDBtag and magType == 'MagDown') :
        e = Exception('Mismatch between supplied magnet type (%s) and CondDB tag (%s)' % (magType,daVinci.CondDBtag))
        raise e


    # build "selection flows"
    from PhysConf.Selections import ( AutomaticData, PrintSelection, MomentumScaling )

    # 1) input selections
    inputSel = AutomaticData( inputLocation )

    # 1a) optional debug printout
    #inputSel = PrintSelection( inputSel )

    # 2) insert momentum scaling - not for simulation!
    #inputSel = MomentumScaling( inputSel )

    # 3) construct Bender selection from structural properties:
    #  - name
    #  - input selection(s)
    #  - all properties of  Bender.Algo  algorithm
    sel_MCTruth = BenderMCSelection (
            mode+'_MCTruth'
            )
    """
    sel_pipi = BenderMCSelection (
            algName+'_pipi' ,       ## the name
            inputs = [ inputSel ] , ## input selection(s)
            PP2MCs = [ 'Relations/Rec/ProtoP/Charged'] ,
            )
    sel_Kpi = BenderMCSelection (
            algName+'_Kpi' ,        ## the name
            inputs = [ inputSel ] , ## input selection(s)
            PP2MCs = [ 'Relations/Rec/ProtoP/Charged'] ,
            )
    sel_KK = BenderMCSelection (
            algName+'_KK' ,         ## the name
            inputs = [ inputSel ] , ## input selection(s)
            PP2MCs = [ 'Relations/Rec/ProtoP/Charged'] ,
            )
    """

    # 4) add the final selections to DaVinci
    #    - it builds the proper algorithm sequences
    #    - the constructed sequences are propertly inserted into the algorithm flow
    daVinci.UserAlgorithms.append( sel_MCTruth )
    #daVinci.UserAlgorithms.append( sel_pipi )
    #daVinci.UserAlgorithms.append( sel_Kpi  )
    #daVinci.UserAlgorithms.append( sel_KK   )

    # only for extremely verbose debugging info!
    #from Configurables import ApplicationMgr
    #ApplicationMgr().OutputLevel = 1

    # ReadGen config
    #from BenderTools.GenFiles import genAction
    #genAction()
    #alg_name = 'ReadGen'
    #daVinci.UserAlgorithms += [genAction]

    setData( datafiles, catalogues )

    gaudi = appMgr()

    conf_MCTruth = {
            'btype'    : btype     ,
            'h1type'   : track1    ,
            'h2type'   : track2    ,
            'ctype'    : ctype     ,
            'gd1type'  : gdaug1    ,
            'gd2type'  : gdaug2    ,
            'filetype' : inputType ,
            }

    from B2Dhh.MCTruthAlgo import B2DhhMCTruth

    algGenMCTruth = B2DhhMCTruth( sel_MCTruth, **conf_MCTruth )

    """
    conf_pipi = {
            'parent_type'          : 'B'                  ,
            'reco_daughters'       : reco_daughters       ,
            'reco_gdaughters'      : [ 'pi', 'pi' ]       ,
            'year'                 : int(dataType)        ,
            'wrongsign'            : False                ,
            'simulation'           : True                 ,
            'signalmc'             : True                 ,
            'mc_daughters'         : mc_daughters         ,
            'mc_gdaughters'        : mc_gdaughters        ,
            'mc_decay_descriptors' : mc_decay_descriptors ,
            }
    conf_Kpi = {
            'parent_type'          : 'B'                  ,
            'reco_daughters'       : reco_daughters       ,
            'reco_gdaughters'      : [ 'K', 'pi' ]        ,
            'year'                 : int(dataType)        ,
            'wrongsign'            : False                ,
            'simulation'           : True                 ,
            'signalmc'             : True                 ,
            'mc_daughters'         : mc_daughters         ,
            'mc_gdaughters'        : mc_gdaughters        ,
            'mc_decay_descriptors' : mc_decay_descriptors ,
            }
    conf_KK = {
            'parent_type'          : 'B'                  ,
            'reco_daughters'       : reco_daughters       ,
            'reco_gdaughters'      : [ 'K', 'K' ]         ,
            'year'                 : int(dataType)        ,
            'wrongsign'            : False                ,
            'simulation'           : True                 ,
            'signalmc'             : True                 ,
            'mc_daughters'         : mc_daughters         ,
            'mc_gdaughters'        : mc_gdaughters        ,
            'mc_decay_descriptors' : mc_decay_descriptors ,
            }
    """

    """
    from B2Dhh.RecoAlgo import B2DhhReco

    algB2Dhh_pipi = B2DhhReco( sel_pipi, **conf_pipi )
    algB2Dhh_Kpi  = B2DhhReco( sel_Kpi,  **conf_Kpi  )
    algB2Dhh_KK   = B2DhhReco( sel_KK,   **conf_KK   )
    """

    return SUCCESS

#############

if '__main__' == __name__ :

    if len(sys.argv) != 4:
        sys.exit("Usage: %s <gdaug1> <gdaug2> <DST>" % sys.argv[0])

    validGDaugs = [ 'pipi', 'Kpi', 'KK' ]
    gDaugStr = '%s%s' % ( sys.argv[1], sys.argv[2] )
    if gDaugStr not in validGDaugs :
        sys.exit("Invalid grand-daughter combination: %s" % gDaugStr)

    datafiles = [ sys.argv[3] ]

    pars = {}

    pars[ 'btype'     ] = 'Bd'
    pars[ 'track1'    ] = 'pi'
    pars[ 'track2'    ] = 'pi'
    pars[ 'ctype'     ] = 'D0'
    pars[ 'gdaug1'    ] = sys.argv[1]
    pars[ 'gdaug2'    ] = sys.argv[2]
    pars[ 'magType'   ] = 'MagDown'
    pars[ 'stripping' ] = 'Stripping34'
    pars[ 'stream'    ] = 'ALLSTREAMS'
    pars[ 'inputType' ] = 'XGEN'
    pars[ 'dddbtag'   ] = 'dddb-20170721-3'
    pars[ 'conddbtag' ] = 'sim-20190430-vc-md100'
    pars[ 'printFreq' ] = 1000
    pars[ 'simType'   ] = 'Sim09f'

    configure( datafiles, params = pars, castor=False )

    run(-1)

#############


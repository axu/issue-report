from TCKUtils.utils import getProperties

from hlt1_2017 import tcks

# check the prescale of Hlt1TrackMVA
for tck in tcks:
    prop = getProperties(tck,'.*/Hlt1TrackMVAPreScaler')
    print( '{0} AcceptFraction: {1}'.format(hex(tck),prop.values()[0]['AcceptFraction']) )

# check threshhold settings of Hlt1TrackMVA
for tck in tcks:
    prop = getProperties(tck,'.*/Hlt1TrackMVAUnit')
    print( '{0} Hlt1TrackMVA'.format(hex(tck)) )
    print( prop.values()[0]['Code'] )


###
#  produce ROOT files of various Xiccp modes
###

# -----
# modes
# -----

lines = [
        [ 'Xiccp2Xic0Pim_Xic0ToPpKmKmPip', 'WS' ],
        [ 'Xiccp2Xic0Pip_Xic0ToPpKmKmPip', 'RS' ],
        [ 'Xiccp2Xic0Kp_Xic0ToPpKmKmPip', 'RS' ],
]
names = [ l[0].split('_')[0]+'_'+l[1] for l in lines ]
print( names )
locations = [ 'Hlt2CharmHad{0}Turbo/Particles'.format(l[0]) for l in lines ]
mass_hc = [
        'Xi_c0', 'Xi_c0', 'Xi_c0' #2470.91, 2470.91
]
descriptors = [
        '${Xicc}[Xi_cc+ -> ${Hc}(Xi_c0 -> ${HcPp}p+ ${HcKm1}K- ${HcKm2}K- ${HcPip}pi+) ${XiccPip}pi-]CC',
        '${Xicc}[Xi_cc+ -> ${Hc}(Xi_c0 -> ${HcPp}p+ ${HcKm1}K- ${HcKm2}K- ${HcPip}pi+) ${XiccPip}pi+]CC',
        '${Xicc}[Xi_cc+ -> ${Hc}(Xi_c0 -> ${HcPp}p+ ${HcKm1}K- ${HcKm2}K- ${HcPip}pi+) ${XiccKp}K+]CC',
]


# -------------
# Create ntuple
# -------------
from os import environ
from GaudiKernel.SystemOfUnits import *
from Gaudi.Configuration import *
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from Configurables import DecayTreeTuple, EventTuple
from DecayTreeTuple.Configuration import *
from Configurables import (
        TupleToolTrigger,
        TupleToolTISTOS,
        TupleToolPrimaries,
        TupleToolDecay,
        TupleToolVtxIsoln,
        TupleToolPid,
        TupleToolRecoStats,
        TupleToolVeto,
        TupleToolDecayTreeFitter
        )
from Configurables import BackgroundCategory, EventCountHisto
from Configurables import LoKi__Hybrid__TupleTool, LoKi__Hybrid__EvtTupleTool


# TupleTools
MyToolList = [
     "TupleToolL0Data"
    ,"TupleToolKinematic"
    ,"TupleToolPid"
    ,"TupleToolTrackInfo"
    ,"TupleToolPrimaries"
    ,"TupleToolPropertime"
    ,"TupleToolEventInfo"
    ,"TupleToolTrackInfo"
    ,"TupleToolRecoStats"
    ,"TupleToolGeometry"
]

# Trigger list
triggerListL0 = [ "L0GlobalDecision"
                  ,"L0PhysDecision"
                  ,"L0HadronDecision"
                  ,"L0MuonDecision"
                  ,"L0MuonHighDecision"
                  ,"L0DiMuonDecision"
                  ,"L0PhotonDecision"
                  ,"L0ElectronDecision" ]
triggerListHlt1 = [ "Hlt1TrackMVADecision"
                    ,"Hlt1TrackMVALooseDecision"
                    ,"Hlt1TwoTrackMVADecision"
                    ,"Hlt1TwoTrackMVALooseDecision"
                    ,"Hlt1L0AnyDecision"
                    ,"Hlt1MBNoBiasDecision" ]
# Together
myTriggerList = triggerListL0 + triggerListHlt1

# LoKi
LoKi_Kine = LoKi__Hybrid__TupleTool("LoKi_Kine")
LoKi_Kine.Variables = {
   "MAXDOCA" : "DOCAMAX",
   "DOCA12"  : "DOCACHI2MAX",
   "Y"    : "Y",
   "ETA"  : "ETA",
}


tupleListAll = [ DecayTreeTuple( name ) for name in names ]
for ii in range(len(lines)):
    dtt = tupleListAll[ii]
    dtt.Inputs = [ locations[ii] ]
    dtt.setDescriptorTemplate( descriptors[ii] )

    # Primaries
    dtt.addTool(TupleToolPrimaries)
    dtt.TupleToolPrimaries.InputLocation= "/Event/Turbo/Primary"

    # TISTOS for Xicc
    dtt.addTool(TupleToolDecay, name='Xicc')
    dtt.Xicc.ToolList += [ "TupleToolTISTOS" ]
    dtt.Xicc.addTool(TupleToolTISTOS, name="TupleToolTISTOS" )
    dtt.Xicc.TupleToolTISTOS.Verbose=True
    dtt.Xicc.TupleToolTISTOS.VerboseHlt1=True
    dtt.Xicc.TupleToolTISTOS.VerboseHlt2=True
    dtt.Xicc.TupleToolTISTOS.TriggerList = myTriggerList

    # TISTOS for Hc
    dtt.addTool(TupleToolDecay, name='Hc')
    dtt.Hc.ToolList += [ "TupleToolTISTOS" ]
    dtt.Hc.addTool(TupleToolTISTOS, name="TupleToolTISTOS" )
    dtt.Hc.TupleToolTISTOS.Verbose=True
    dtt.Hc.TupleToolTISTOS.VerboseHlt1=True
    dtt.Hc.TupleToolTISTOS.VerboseHlt2=True
    dtt.Hc.TupleToolTISTOS.TriggerList = myTriggerList

    # DecayTreeFitter
    # The Verbose option should create variables "for the head particle and its daughters too"
    # There is a further UpdateDaughters option "to store the information from those tracks"
    # do nothing: implement momentum scale calibration
    dtt.Xicc.ToolList += ["TupleToolDecayTreeFitter/PVFit0"]
    dtt.Xicc.addTool(TupleToolDecayTreeFitter("PVFit0"))
    dtt.Xicc.PVFit0.Verbose = True
    dtt.Xicc.PVFit0.UpdateDaughters = True

    # constrain C PV
    dtt.Xicc.ToolList += ["TupleToolDecayTreeFitter/PVFit1"]
    dtt.Xicc.addTool(TupleToolDecayTreeFitter("PVFit1"))
    dtt.Xicc.PVFit1.Verbose = True
    dtt.Xicc.PVFit1.constrainToOriginVertex = True
    dtt.Xicc.PVFit1.UpdateDaughters = True

    # constrain Hc mass
    dtt.Xicc.ToolList += ["TupleToolDecayTreeFitter/PVFit2"]
    dtt.Xicc.addTool(TupleToolDecayTreeFitter("PVFit2"))
    dtt.Xicc.PVFit2.Verbose = True
    dtt.Xicc.PVFit2.UpdateDaughters = True
    dtt.Xicc.PVFit2.daughtersToConstrain = [ mass_hc[ii] ]

    # constrain C PV and Hc mass
    dtt.Xicc.ToolList += ["TupleToolDecayTreeFitter/PVFit3"]
    dtt.Xicc.addTool(TupleToolDecayTreeFitter("PVFit3"))
    dtt.Xicc.PVFit3.Verbose = True
    dtt.Xicc.PVFit3.constrainToOriginVertex = True
    dtt.Xicc.PVFit3.UpdateDaughters = True
    dtt.Xicc.PVFit3.daughtersToConstrain = [ mass_hc[ii] ]

	# LoKi Tool
    dtt.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Kine"]
    dtt.addTool(LoKi_Kine)

    ## Obsolete 
    #dtt.RootInTES = "/Event/Charmmultibody/Turbo"
    #dtt.WriteP2PVRelations = False

    dtt.ToolList += MyToolList

## Necessary from DaVinci v40r1 onward
#from Configurables import DstConf, TurboConf
#TurboConf().PersistReco=True
#DstConf().Turbo=True

# Momentum scale
from PhysConf.Selections import MomentumScaling, AutomaticData, TupleSelection
my_sels = [ AutomaticData( location ) for location in locations ]
#my_sels = [
#     AutomaticData ( "Hlt2CharmHadXiccp2LcpKmPip_Lcp2PpKmPipTurbo/Particles"),
#     AutomaticData ( "Hlt2CharmHadXiccp2LcpKmPim_Lcp2PpKmPipTurbo/Particles"),
#     AutomaticData ( "Hlt2CharmHadXiccp2LcpKpPim_Lcp2PpKmPipTurbo/Particles"),
#     ]
my_selection = [ MomentumScaling ( sel, Turbo = True , Year = '2018' ) for sel in my_sels ]
from PhysConf.Selections import SelectionSequence
selseq = [ SelectionSequence('MomScale'+str(ii), my_selection[ii]).sequence() for ii in range(len(my_selection))]
#selseq = [ SelectionSequence ('MomScale', sel).sequence() for sel in my_selection ]

# ---------------------
# DaVinci configuration
# ---------------------
from Configurables import DaVinci
# Database
from Configurables import CondDB
CondDB ( LatestGlobalTagByDataType = '2018')
#DaVinci().DDDBtag = "dddb-20150526"
#DaVinci().CondDBtag = "cond-20170510"
DaVinci().UserAlgorithms += selseq + tupleListAll
DaVinci().PrintFreq = 5000
DaVinci().SkipEvents = 0
DaVinci().Simulation = False
DaVinci().Lumi = not DaVinci().Simulation
DaVinci().EvtMax = -1
DaVinci().DataType = '2018'
DaVinci().InputType = 'MDST'
DaVinci().RootInTES = '/Event/Charmmultibody/Turbo'
DaVinci().Turbo = True

# Output
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().TupleFile = 'Tuple.root'

# XML summary
from Configurables import LHCbApp
LHCbApp().XMLSummary="summary.xml"

# ---------------
# Local test only
TEST = False
if TEST:
    DaVinci().EvtMax = 10000
    DaVinci().PrintFreq = 1000
    from GaudiConf import IOHelper
    IOHelper().inputFiles([
      '/afs/cern.ch/user/a/axu/grid/LHCb/Collision18/CHARMMULTIBODY.MDST/00075226/0000/00075226_00000015_1.charmmultibody.mdst'
     ,'/afs/cern.ch/user/a/axu/grid/LHCb/Collision18/CHARMMULTIBODY.MDST/00075226/0000/00075226_00000027_1.charmmultibody.mdst'
     ,'/afs/cern.ch/user/a/axu/grid/LHCb/Collision18/CHARMMULTIBODY.MDST/00075226/0000/00075226_00000045_1.charmmultibody.mdst'
     ,'/afs/cern.ch/user/a/axu/grid/LHCb/Collision18/CHARMMULTIBODY.MDST/00075226/0000/00075226_00000051_1.charmmultibody.mdst'
     ,'/afs/cern.ch/user/a/axu/grid/LHCb/Collision18/CHARMMULTIBODY.MDST/00075226/0000/00075226_00000063_1.charmmultibody.mdst'
    ], clear=True)

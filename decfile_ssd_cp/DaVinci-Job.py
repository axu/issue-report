from os import environ
import math

from GaudiKernel.SystemOfUnits import *
from Gaudi.Configuration import *
from DecayTreeTuple.Configuration import *
from Configurables import MCDecayTreeTuple, GaudiSequencer
from Configurables import PrintMCTree

# MCDecayTree
mcdtt = MCDecayTreeTuple('mcntuple')
decay = '${B0}[B0 ==> ${D0}([D0]cc ==> ${Kp}K+ ${Km}K-) ${rho}(rho(770)0 ==> ${Pip}pi+ ${Pim}pi-)]CC'
#decay = '${B0}[B0 ==> ${B0bar}B~0]CC'
mcdtt.setDescriptorTemplate( decay )
mcdtt.ToolList = [
    #'MCTupleToolKinematic',
    #'MCTupleToolHierarchy', ]
    #"MCTupleToolKinematic",
    "MCTupleToolHierarchy",
    "MCTupleToolPID",
    #"MCTupleToolPrimaries",
    #"MCTupleToolEventType",
    #"MCTupleToolAngles",
    #"MCTupleToolPrompt",
    #"MCTupleToolReconstructed"
]
mcdtt.addTupleTool("MCTupleToolKinematic").Verbose = True

# print decay tree
decay_heads = ["B0", "B~0"]
printMC = PrintMCTree()
printMC.ParticleNames = decay_heads

# Cope with error
def doIt():
    '''
    specific post-config action for (x)GEN-files
    '''
    extension = 'xgen'
    ext = extension.upper()

    from Configurables import DataOnDemandSvc
    dod  = DataOnDemandSvc ()
    from copy import deepcopy 
    algs = deepcopy ( dod.AlgMap ) 
    bad  = set() 
    for key in algs :
        if     0 <= key.find ( 'Rec'     )                  : bad.add ( key )
        elif   0 <= key.find ( 'Raw'     )                  : bad.add ( key )
        elif   0 <= key.find ( 'DAQ'     )                  : bad.add ( key )
        elif   0 <= key.find ( 'Trigger' )                  : bad.add ( key )
        elif   0 <= key.find ( 'Phys'    )                  : bad.add ( key )
        elif   0 <= key.find ( 'Prev/'   )                  : bad.add ( key )
        elif   0 <= key.find ( 'Next/'   )                  : bad.add ( key )
        elif   0 <= key.find ( '/MC/'    ) and 'GEN' == ext : bad.add ( key )
        
    for b in bad :
        del algs[b]
            
    dod.AlgMap = algs
    
    from Configurables import EventClockSvc, CondDB 
    EventClockSvc ( EventTimeDecoder = 'FakeEventTime' )
    CondDB  ( IgnoreHeartBeat = True )
    
appendPostConfigAction( doIt )

# xgen input
from Configurables import EventSelector
datafile = 'Gauss-10000ev-20201015.xgen'
#datafile = 'Gauss-1000000ev-20201014.xgen'
#datafile = 'Gauss-100ev-20201014.xgen'
EventSelector().Input = ["DATAFILE='{0}' TYP='POOL_ROOTTREE' Opt='READ'".format(datafile)]

SeqPhys = GaudiSequencer('SeqPhys')
SeqPhys.Members += [mcdtt]

from Configurables import DaVinci
DaVinci().EvtMax = -1
DaVinci().PrintFreq = 1000
DaVinci().SkipEvents = 0
DaVinci().DataType = '2012'
DaVinci().Simulation = True
DaVinci().Lumi = not DaVinci().Simulation
DaVinci().HistogramFile = 'DVHistos.root'
DaVinci().TupleFile = 'Tuple.root'
DaVinci().UserAlgorithms = [ SeqPhys ]
#DaVinci().UserAlgorithms = [ printMC, SeqPhys ]

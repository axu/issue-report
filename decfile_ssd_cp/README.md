`Bd_D0rho0,KK=SSD_CP,DecProdCut.dec` is the decfile intended to generate
`B0 -> D~0 (-> K+ K-) rho0 (-> pi+ pi-)` decays with time-dependent CPV.

Generator level particle gun MC sample is generated for test
and the output `.xgen` file is analysed with DaVinci.
The command used for generation and analysis sits in `run.sh`.

Two issues are observed:
1. According to particle `ID`, `N(B0):N(B0bar)=2:1`, which we expect to be equal.
2. For events in which B0 and D0 particle ID have the same sign 
(in which case we expect that the B0 has oscillated),
the oscillation flag `OSCIL` can have values of both 0 and 1, which is not reasonable.

The above issues do not appear if we generate the same decay without the use of `SSD_CP` model.

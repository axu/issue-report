# run gauss
gaudirun.py ./Gauss-Job.py $GAUSSOPTS/Gauss-2012.py $LBPGUNSROOT/options/PGuns.py ./BParticleGun.py $GAUSSOPTS/GenStandAlone.py $LBPYTHIA8ROOT/options/Pythia8.py

# run davinci
lb-run davinci/v45r0 gaudirun.py DaVinci-Job.py

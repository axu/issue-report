def fillTuple( tuple, myBranches, myTriggerList ):

    tuple.Branches = myBranches

    tuple.ToolList = [
        "TupleToolAngles",
        "TupleToolEventInfo",
        "TupleToolGeometry",
        "TupleToolKinematic",
        "TupleToolPid",
        "TupleToolANNPID" ,
        "TupleToolPrimaries",
        "TupleToolPropertime",
        "TupleToolRecoStats",
        "TupleToolTrackInfo",
        "TupleToolL0Data",
        ]

    # RecoStats for filling SpdMult, etc                                                     
    from Configurables import TupleToolRecoStats
    tuple.addTool(TupleToolRecoStats, name="TupleToolRecoStats")
    tuple.TupleToolRecoStats.Verbose=True

    from Configurables import TupleToolTISTOS, TupleToolDecay
    tuple.addTool(TupleToolDecay, name = 'Xic0')
#    tuple.addTool(TupleToolDecay, name = 'KS')                                              

    # TISTOS
    tistos = tuple.addTupleTool('TupleToolTISTOS')
    tistos.TriggerList = myTriggerList
    tistos.FillHlt2 = False
    tistos.Verbose = True
    tuple.Xic0.ToolList+=[ "TupleToolTISTOS" ]
    tuple.Xic0.addTool(TupleToolTISTOS, name="TupleToolTISTOS" )
    tuple.Xic0.TupleToolTISTOS.Verbose=True
    tuple.Xic0.TupleToolTISTOS.TriggerList = myTriggerList

    from Configurables import LoKi__Hybrid__TupleTool
    LoKi_All=LoKi__Hybrid__TupleTool("LoKi_All")
    LoKi_All.Variables = {
        "ETA"                  : "ETA",
        "Y"                    : "Y"  ,
        "LOKI_IPCHI2"          : "BPVIPCHI2()",
        "MAXDOCA"              : "DOCAMAX"
        }
    tuple.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_All"]
    tuple.addTool(LoKi_All)

    fit = tuple.Xic0.addTupleTool('TupleToolDecayTreeFitter/PVFit0')
    #fit.constrainToOriginVertex = True
    fit.Verbose= True
    fit.UpdateDaughters = True
    # fit.daughtersToConstrain += ['p+']



from Configurables import DecayTreeTuple
from Configurables import TupleToolTrigger, TupleToolTISTOS,TupleToolPrimaries
from Configurables import TupleToolVtxIsoln, TupleToolPid, TupleToolRecoStats, TupleToolVeto, TupleToolDecayTreeFitter
from DecayTreeTuple.Configuration import *

myTriggerList = [
    # L0                                                                                     
    "L0ElectronDecision",
    "L0PhotonDecision",
    "L0HadronDecision",
    "L0MuonDecision",
    "L0MuonHighDecision",
    "L0DiMuonDecision",
    "L0PhysDecision",
    "L0GlobalDecision",

    # Hlt1 track                                                                             
    "Hlt1TrackAllL0Decision",
    "Hlt1TrackMuonDecision",
    "Hlt1TrackPhotonDecision",
    "Hlt1GlobalDecision",

    "Hlt1TrackMVADecision",
    "Hlt1TwoTrackMVADecision"
    ]

	
Xic02PpKmKmPip_Branch = {
    "Xic0"    :  "^([ Xi_c0 -> p+ K- K- pi+ ]CC)",
    "Pp"    :  "[ Xi_c0 -> ^p+ K- K- pi+ ]CC",
    "Km1"    :  "[ Xi_c0 -> p+ ^K- K- pi+ ]CC",
    "Km2"    :  "[ Xi_c0 -> p+ K- ^K- pi+ ]CC",
    "Pip"    :  "[ Xi_c0 -> p+ K- K- ^pi+ ]CC"
}

the_line = "Hlt2CharmHadXic0ToPpKmKmPipTurbo/Particles"
Xic02PpKmKmPip = DecayTreeTuple("Xic02PpKmKmPip")
#Xic02PpKmKmPip.WriteP2PVRelations = False
#Xic02PpKmKmPip.RootInTES = "/Event/Turbo"
Xic02PpKmKmPip.Decay ="[ Xi_c0 -> ^p+ ^K- ^K- ^pi+ ]CC"
Xic02PpKmKmPip.Inputs = [ the_line ]
fillTuple( Xic02PpKmKmPip,
           Xic02PpKmKmPip_Branch,
           myTriggerList )



## Tighten Trk Chi2 to <3
#from CommonParticles.Utils import DefaultTrackingCuts
#DefaultTrackingCuts().Cuts  = { "Chi2Cut" : [ 0, 3 ],
#                                "CloneDistCut" : [5000, 9e+99 ] }

# Momentum scale
from PhysConf.Selections import MomentumScaling, AutomaticData, TupleSelection
my_sel = AutomaticData ( the_line )
my_selection = MomentumScaling ( my_sel, Turbo = True, Year = '2018' )
from PhysConf.Selections import SelectionSequence
selseq = SelectionSequence ('MomScale', my_selection).sequence()

#from Configurables import DstConf, TurboConf
#TurboConf().PersistReco=True
#DstConf().Turbo=True

from Configurables import CondDB
CondDB ( LatestGlobalTagByDataType = '2018')

from Configurables import DaVinci
#DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().DataType = '2018'
DaVinci().DDDBtag ="dddb-20171030-3"
DaVinci().CondDBtag = "cond-20180202"
DaVinci().EvtMax =  -1                         # Number of events
DaVinci().SkipEvents = 0                       # Events to skip
DaVinci().PrintFreq = 5000
DaVinci().DataType = "2018"
DaVinci().Simulation    = False
DaVinci().HistogramFile = "DVHistos.root"      # Histogram file
DaVinci().TupleFile = "Tuple.root"             # Ntuple
DaVinci().UserAlgorithms += [ selseq, Xic02PpKmKmPip ]
DaVinci().Lumi = True
DaVinci().InputType = "MDST"
DaVinci().RootInTES = '/Event/Charmmultibody/Turbo'
DaVinci().Turbo = True

#from Configurables import LHCbApp
#LHCbApp().DDDBtag   = "dddb-20150724"
#LHCbApp().CondDBtag = "cond-20170120-1"

# Local test only
DaVinci().EvtMax = -1
DaVinci().PrintFreq = 5000
from GaudiConf import IOHelper
IOHelper().inputFiles([
  'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/LHCb/Collision18/CHARMMULTIBODY.MDST/00080040/0000/00080040_00004442_1.charmmultibody.mdst'
  'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/LHCb/Collision18/CHARMMULTIBODY.MDST/00076507/0000/00076507_00002163_1.charmmultibody.mdst',
  'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/LHCb/Collision18/CHARMMULTIBODY.MDST/00076507/0001/00076507_00012660_1.charmmultibody.mdst',
  'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/LHCb/Collision18/CHARMMULTIBODY.MDST/00080040/0000/00080040_00000180_1.charmmultibody.mdst',
  'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/LHCb/Collision18/CHARMMULTIBODY.MDST/00077052/0000/00077052_00002667_1.charmmultibody.mdst',
  'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/LHCb/Collision18/CHARMMULTIBODY.MDST/00080040/0001/00080040_00014968_1.charmmultibody.mdst',
  'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/LHCb/Collision18/CHARMMULTIBODY.MDST/00077052/0000/00077052_00002450_1.charmmultibody.mdst',
  'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/LHCb/Collision18/CHARMMULTIBODY.MDST/00077052/0001/00077052_00010841_1.charmmultibody.mdst',
  'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/LHCb/Collision18/CHARMMULTIBODY.MDST/00075140/0000/00075140_00007108_1.charmmultibody.mdst',
  'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/LHCb/Collision18/CHARMMULTIBODY.MDST/00077052/0000/00077052_00008578_1.charmmultibody.mdst',
], clear=True)

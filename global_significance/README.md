The two methods refer to:

  * method1: generate toys and fit the f(t0|0) distributions
    * toy_bin.py: the core file of generating toys
    * get_pvalue_global.py: fit f(t0|0) to get global p-values

  * method2: generate small toys to get <N(c0)> in the equation
  that gives a bound to the p-value.
    * toy_analytcial.py: the core file of generating toys
    * add_branch.py: calculate the number of upcrossings
    * get_pvalue_global_analytical.py: calculate the global p-values

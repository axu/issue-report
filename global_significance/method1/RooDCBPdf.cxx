/***************************************************************************** 
 * Project: RooFit                                                           * 
 *                                                                           * 
 * This code was autogenerated by RooClassFactory                            * 
 *****************************************************************************/ 

// Your description goes here... 

#include "Riostream.h" 

#include "RooDCBPdf.h" 
#include "RooAbsReal.h" 
#include "RooAbsCategory.h" 
#include <math.h> 
#include "TMath.h" 

ClassImp(RooDCBPdf) 

 RooDCBPdf::RooDCBPdf(const char *name, const char *title, 
                        RooAbsReal& _x,
                        RooAbsReal& _mean,
                        RooAbsReal& _sigma,
                        RooAbsReal& _nl,
                        RooAbsReal& _al,
                        RooAbsReal& _nr,
                        RooAbsReal& _ar) :
   RooAbsPdf(name,title), 
   x("x","x",this,_x),
   mean("mean","mean",this,_mean),
   sigma("sigma","sigma",this,_sigma),
   nl("nl","nl",this,_nl),
   al("al","al",this,_al),
   nr("nr","nr",this,_nr),
   ar("ar","ar",this,_ar)
 { 
 } 


 RooDCBPdf::RooDCBPdf(const RooDCBPdf& other, const char* name) :  
   RooAbsPdf(other,name), 
   x("x",this,other.x),
   mean("mean",this,other.mean),
   sigma("sigma",this,other.sigma),
   nl("nl",this,other.nl),
   al("al",this,other.al),
   nr("nr",this,other.nr),
   ar("ar",this,other.ar)
 { 
 } 



 Double_t RooDCBPdf::evaluate() const 
 { 

    double delta = (x-mean)/sigma;
    double tal = TMath::Abs(al);
    double tar = TMath::Abs(ar);
    if(delta<=-tal) return pow(nl/tal,nl)* exp(-tal*tal/2.)*pow(nl/tal-tal-delta,-nl);
    else if(delta>-tal && delta<tar) return exp(-delta*delta/2.);
    else if(delta>=tar) return  pow(nr/tar,nr)* exp(-tar*tar/2.)*pow(nr/tar-tar+delta,-nr);
   // ENTER EXPRESSION IN TERMS OF VARIABLE ARGUMENTS HERE 

   //return 1.0 ; 
 } 




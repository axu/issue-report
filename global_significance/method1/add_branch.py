from ROOT import *

#import numpy as np
#import root_numpy as rn
#from hep_ml.reweight import GBReweighter, BinsReweighter
#import pandas

from array import array
from math import exp, log
import sys, os

def add_branch(input,tree,output):
    # Input
    f_mc = TFile(input)
    t1 = f_mc.Get(tree)

    # New branches
    f_new = TFile(output,"RECREATE")
    tree_new = t1.CloneTree(0)
    t0_global = array('d',[.0])
    tree_new.Branch("t0_global", t0_global, "t0_global/D")
    t0_local = array('d',[.0])
    tree_new.Branch("t0_local", t0_local, "t0_local/D")
    
    # Loop and add branches
    print("Looping...")
    Nentries = t1.GetEntries()
    print(Nentries)
    for ii in xrange(Nentries):
        t1.GetEntry(ii)
        if ii%1000==0:
            print("Processing: " + "\t" + str(ii))
        if t1.N_sig>0:
            t0_global[0] = 2*( t1.NLL_bkg - t1.NLL_tot )
        else:
            t0_global[0] = 0
        if t1.N_sig_local>0:
            t0_local[0] = 2*( t1.NLL_bkg - t1.NLL_tot_local )
        else:
            t0_local[0] = 0
        tree_new.Fill()

    # Save and quit
    f_new.cd()
    tree_new.Write()
    f_new.Close()

add_branch("toy_debug.root","toy","toy_debug_t0.root")
#add_branch("toy_test.root","toy","toy_test_t0.root")
#add_branch("toy.root","toy","toy_t0.root")

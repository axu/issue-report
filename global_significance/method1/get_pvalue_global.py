"""
fit the t0 distribution and calculate the p-value by integral
"""

import math

import ROOT as rt
from ROOT import RooFit as rf

import RS_unbin_tot_Run12_2step_upper_limit as tot_model

import lhcbStyle

rt.gROOT.SetBatch(True)

f1 = rt.TFile('toy_bin_t0.root')
t1 = f1.Get('toy')
t0 = rt.RooRealVar("t0_global","t0_global", 0., 25.)
ds = rt.RooDataSet("ds","ds", t1, rt.RooArgSet(t0))

gamma = rt.RooRealVar("gamma","gamma", 5.,0.,20.)
beta = rt.RooRealVar("beta","beta", 2.,0.,5.)
mu = rt.RooRealVar("mu","mu",0)
#gamma.setConstant(True)
#beta.setConstant(True)
mu.setConstant(True)
#tot = rt.RooGamma("chisq","chisq", t0, gamma, beta, mu)
chisq = rt.RooGamma("chisq","chisq", t0, gamma, beta, mu)

delta = rt.RooTruthModel("delta","delta", t0)

frac = rt.RooRealVar("frac","frac",0.5)
tot = rt.RooAddPdf("tot","tot", rt.RooArgList(chisq,delta), rt.RooArgList(frac))

tot.fitTo(ds)

c1 = rt.TCanvas("c1","c1",900,600)
c1.SetLogy()
tframe = t0.frame()
#ds.plotOn(tframe)
ds.plotOn(tframe,rf.Binning(50))
tot.plotOn(tframe)
tframe.GetYaxis().SetTitle("Ntoys #times f(t_{0}|0)")
tframe.SetMinimum(1)
tframe.GetXaxis().SetTitle("#it{t_{0}}")
t0_exp = 2*(tot_model.minNLL_bkg-tot_model.minNLL_tot)
line = rt.TLine( t0_exp, tframe.GetMinimum(), t0_exp, tframe.GetMaximum())
line.SetLineColor(rt.kRed)
line.SetLineStyle(rt.kSolid)
tframe.addObject(line)
tframe.Draw("hist")
c1.SaveAs("pglobal_Run12_1step.pdf")

vmu = mu.getVal()
vbeta = beta.getVal()
vgamma = gamma.getVal()
#inteall = ROOT.Math.gamma_cdf(pow(10.,10.), vgamma, vbeta, vmu)
intesig1 = rt.Math.gamma_cdf(t0_exp, vgamma, vbeta, vmu)
pvalue = 0.5*(1.-intesig1)
sigma1 = -1.*rt.TMath.NormQuantile(pvalue)
sigma2 = rt.TMath.NormQuantile(1.-pvalue)

print( "intesig1 = {0}".format(intesig1) )
print( "pvalue   = {0}".format(pvalue) )
print( "sigma1   = {0}".format(sigma1) )
print( "sigma2   = {0}".format(sigma2) )

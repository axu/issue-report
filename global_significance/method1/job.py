"""
option file to submit ganga jobs
"""

import os
import sys

path = '/afs/cern.ch/user/a/axu/workdir/private/Xiccp2LcKPi/significance/significance_unbin_tot_Run12_2step_upper_limit'
script = 'toy.py'
script = os.path.join( path, script )
inputs = ['RooDCBPdf.h','RooDCBPdf.cxx','tau_nominal.py',
          'RS_unbin_bkg_Run12_2step_upper_limit.py',
          'RS_unbin_bkg_Run12_2step_upper_limit.py']
inputs = [os.path.join( path,f ) for f in inputs]

# configure root
rt = Root(
        version = '6.12.06',
        usepython = True,
        script = script,
)

j = Job()
j.name = 'Run12_2step_upper_limit'

j.application = rt

njobs = 500
j.splitter = GenericSplitter()
j.splitter.attribute = 'application.args'
j.splitter.values = [ [ii] for ii in range(njobs) ]

j.inputfiles = inputs
j.outputfiles = [ LocalFile('toy.root') ]

#j.backend = Local()
j.backend = Dirac()

j.submit()

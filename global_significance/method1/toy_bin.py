"""
Evaluate the global significance in the case where we get a 3 sigma excess:
    Run1+Run2 with 2-step 80fs MVA
"""

import math
import sys, os
from array import array
import time
import re
from multiprocessing import Pool, cpu_count

import ROOT as RT
from ROOT import RooFit as RF

import tau_nominal as para_mc
import RS_unbin_bkg_Run12_2step_upper_limit as bkg_model
import RS_unbin_tot_Run12_2step_upper_limit as tot_model

RT.gROOT.SetBatch(True)

DRAW = False

# Load the parameters manually for now
free_mean = tot_model.meanXicc
free_meanerr = tot_model.meanXicc_error
fix_sigma = para_mc.wSigma
fix_ratio = para_mc.ratio
lower, upper = 3400., 3800.
scan_lower, scan_upper = 3500., 3700.
nscan = 80
step_size = 1.*(scan_upper-scan_lower)/nscan
fit_a0 = bkg_model.a0
fit_a1 = bkg_model.a1
ntot = bkg_model.nSig + bkg_model.nBkg

def play(arglist):
    start_time = time.time()
    njob,ntoy = arglist[0], arglist[1]
    # variable
    x = RT.RooRealVar("x","", lower, upper)
    x.setBins(800)
    # signal model
    meanXicc = RT.RooRealVar("meanXicc","", free_mean, lower, upper)
    wSigma = RT.RooRealVar("wSigma","", para_mc.wSigma)
    f = RT.RooRealVar("f","", para_mc.f)
    # ratio = SigmaB/SigmaA
    ratio = RT.RooRealVar("ratio","", para_mc.ratio)
    SigmaA = RT.RooFormulaVar( "SigmaA","wSigma/sqrt(f+(1.-f)*ratio*ratio)", RT.RooArgList(wSigma,f,ratio) )
    SigmaB = RT.RooFormulaVar( "SigmaB","wSigma/sqrt(f/ratio/ratio+(1.-f))", RT.RooArgList(wSigma,f,ratio) )
    gaussXicc = RT.RooGaussian("gaussXicc","gaussXicc", x, meanXicc, SigmaA)
    RT.gROOT.ProcessLine(".x RooDCBPdf.cxx")
    nL = RT.RooRealVar("nL","", para_mc.nL)
    aL = RT.RooRealVar("aL","", para_mc.aL)
    nR = RT.RooRealVar("nR","", para_mc.nR)
    aR = RT.RooRealVar("aR","", para_mc.aR)
    cbXicc = RT.RooDCBPdf("cbXicc", "cbXicc", x, meanXicc, SigmaB, nL, aL, nR, aR)
    sig = RT.RooAddPdf("sig","sig", RT.RooArgList(gaussXicc,cbXicc), RT.RooArgList(f))
    nSig = RT.RooRealVar("nSig","", 0., -0.01*ntot, 0.01*ntot)
    sig_pdf = RT.RooExtendPdf("sig_pdf","sig_pdf", sig, nSig)
    # bkg free model: used to fit
    a0 =  RT.RooRealVar("a0","a0", fit_a0, -1.0, 1.0)
    a1 =  RT.RooRealVar("a1","a1", fit_a1, -1.0, 1.0)
    bkg = RT.RooChebychev("bkg","bkg", x, RT.RooArgList(a0,a1))
    nBkg = RT.RooRealVar("nBkg","", ntot, 0.5*ntot, 1.5*ntot)
    bkg_pdf = RT.RooExtendPdf("bkg_pdf","bkg_pdf", bkg, nBkg)
    # total
    tot_pdf = RT.RooAddPdf("tot_pdf","", RT.RooArgList(sig_pdf,bkg_pdf))

    # record the results with ROOT file
    #f1 = RT.TFile('toy_{0}.root'.format(njob),'recreate')
    f1 = RT.TFile('toy_bin.root','recreate')
    t1 = RT.TTree('toy','toy')
    status_bkg = array('d',[0.])
    t1.Branch('status_bkg',status_bkg,'status_bkg/D')
    covQual_bkg = array('d',[0.])
    t1.Branch('covQual_bkg',covQual_bkg,'covQual_bkg/D')
    NLL_bkg = array('d',[0.])
    t1.Branch('NLL_bkg',NLL_bkg,'NLL_bkg/D')
    NLL_tot = array('d',[0.])
    t1.Branch('NLL_tot',NLL_tot,'NLL_tot/D')
    NLL_tot_local = array('d',[0.])
    t1.Branch('NLL_tot_local',NLL_tot_local,'NLL_tot_local/D')
    Mean = array('d',[0.])
    t1.Branch('Mean',Mean,'Mean/D')
    N_sig = array('d',[0.])
    t1.Branch('N_sig',N_sig,'N_sig/D')
    N_sig_local = array('d',[0.])
    t1.Branch('N_sig_local',N_sig_local,'N_sig_local/D')
    N_gen = array('d',[0.])
    t1.Branch('N_gen',N_gen,'N_gen/D')
    toy_seed = array('d',[0.])
    t1.Branch('toy_seed',toy_seed,'toy_seed/D')
    toy_number = array('d',[0.])
    t1.Branch('toy_number',toy_number,'toy_number/D')

    # generate toy
    # bkg fixed model
    a0_toy =  RT.RooRealVar("a0_toy","a0_toy", fit_a0)
    a1_toy =  RT.RooRealVar("a1_toy","a1_toy", fit_a1)
    bkg_toy = RT.RooChebychev("bkg_toy","bkg_toy", x, RT.RooArgList(a0_toy,a1_toy))
    seed = 0
    RT.gRandom.SetSeed(seed)
    toy_seed[0] = RT.gRandom.GetSeed()
    for ii in range(ntoy):
        toy_number[0] = ii
        print('###')
        print('Playing toy {0}...'.format(ii))
        print('###')
        gen_tot = RT.gRandom.Poisson(ntot)
        N_gen[0] = gen_tot
        print('Generating pseudo-sample with {0} events...'.format(gen_tot))
        #ds = bkg.generate( RT.RooArgSet(x), gen_tot )
        ds = bkg_toy.generateBinned( RT.RooArgSet(x), gen_tot )
        ds.Print()
        print('Pseudo-sample generated!')
        # bkg only fit
        nSig.setVal(0)
        nSig.setConstant(True)
        res = tot_pdf.fitTo( ds, RF.Save(True), RF.PrintLevel(-1) )
        status_bkg[0] = res.status()
        covQual_bkg[0] = res.covQual()
        NLL_bkg[0] = tot_pdf.createNLL( ds, RF.Extended(True) ).getVal()
        # local significance
        meanXicc.setRange(free_mean-free_meanerr, free_mean+free_meanerr)
        meanXicc.setConstant(False)
        nSig.setVal(0)
        nSig.setConstant(False)
        nSig.setRange( -0.01*ntot, 0.01*ntot )
        tot_pdf.fitTo( ds, RF.PrintLevel(-1) )
        NLL = tot_pdf.createNLL( ds, RF.Extended(True) ).getVal()
        NLL_tot_local[0] = NLL
        N_sig_local[0] = nSig.getVal()
        if DRAW:
            can = RT.TCanvas("can","can",600,400)
            massframe = x.frame()
            ds.plotOn( massframe, RF.Name("ds") )
            tot_pdf.plotOn( massframe, RF.Components("bkg_pdf"), RF.Name("bkg"),
                RF.LineColor(RT.kBlack), RF.LineStyle(RT.kDashed) )
            tot_pdf.plotOn( massframe, RF.Components("sig_pdf"), RF.Name("signal"),
                RF.LineColor(RT.kRed) )
            tot_pdf.plotOn( massframe, RF.Name("tot"),
                RF.LineColor(RT.kBlue) )
            massframe.Draw()
            can.SaveAs('toy_tot_{0}_{1}.pdf'.format(seed,ii))
        # global significance
        # scan the mass region
        minNLL = 10e10
        minSig = -1000.
        minMean = 3620.
        scan_mass = [scan_lower+jj*step_size for jj in range(nscan+1)]
        meanXicc.setRange(lower, upper)
        meanXicc.setConstant(False)
        for peak in scan_mass:
            #print('Scanning peak {0}...'.format(peak))
            meanXicc.setVal(peak)
            meanXicc.setConstant(True)
            nSig.setVal(0)
            nSig.setConstant(False)
            nSig.setRange( -0.01*ntot, 0.01*ntot )
            tot_pdf.fitTo( ds, RF.PrintLevel(-1) )
            NLL = tot_pdf.createNLL( ds, RF.Extended(True) ).getVal()
            if NLL<minNLL:
                minNLL = NLL
                minSig = nSig.getVal()
                minMean = peak
            #print('Peak   = {0}'.format(peak))
            #print('NLL    = {0}'.format(NLL))
            #print('nSig   = {0}'.format(nSig.getVal()))
            #print('minPeak= {0}'.format(minMean))
            #print('minNLL = {0}'.format(minNLL))
            #print('minSig = {0}'.format(minSig))
        Mean[0] = minMean
        N_sig[0] = minSig
        NLL_tot[0] = minNLL
        t1.Fill()
    t1.Write()
    f1.Close()
    print("--- {0} seconds spent ---".format(time.time()-start_time))

if __name__ == '__main__':
    # grid
    ntoy = 100
    config = [int(sys.argv[1]),int(ntoy)]
    play(config)
    """
    # local
    ntoy = 5
    njobs = 100
    pool = Pool( processes=njobs )
    data = [ [njob,ntoy] for njob in range(njobs) ]
    data = tuple(data)
    pool.map( play, data )
    # add the output
    hadd = 'hadd -f {0}'.format('toy.root')
    clean = 'rm -f'
    pattern = re.compile('toy_[0-9]+\.root')
    output_path = '.'
    for f in os.listdir(output_path):
        print f
        if pattern.match(f):
            hadd += ' '+f
            clean += ' '+f
    print hadd
    os.system( hadd )
    print clean
    os.system( clean )
    """


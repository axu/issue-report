"""
Calculate the p-value
"""

import math

import ROOT as rt
from ROOT import RooFit as rf

import RS_unbin_tot_Run12_2step_upper_limit as tot_model

f1 = rt.TFile('toy_t0.root')
t1 = f1.Get('toy')

# get <N(c0)>
nupcrossing = []
for candidate in t1:
    nupcrossing.append( candidate.nupcrossing )
mean = 1.0*sum(nupcrossing)/len(nupcrossing)

c = 2*(tot_model.minNLL_bkg-tot_model.minNLL_tot)
c0 = 0.5
s = 1
pvalue = rt.TMath.Prob( c, s ) + mean*math.exp(-(c-c0)/2)
sigma1 = -1.*rt.TMath.NormQuantile(pvalue)
sigma2 = rt.TMath.NormQuantile(1.-pvalue)

print( "pvalue   = {0}".format(pvalue) )
print( "sigma1   = {0}".format(sigma1) )
print( "sigma2   = {0}".format(sigma2) )

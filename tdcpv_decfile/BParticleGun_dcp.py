# content of the file BParticleGun.py
from Configurables import ParticleGun, MomentumRange, FlatNParticles, ToolSvc, EvtGenDecay
from GaudiKernel import SystemOfUnits

pgun = ParticleGun()
pgun.ParticleGunTool = "MomentumRange"
pgun.addTool( MomentumRange , name = "MomentumRange" )

pgun.NumberOfParticlesTool = "FlatNParticles"
pgun.addTool( FlatNParticles , name = "FlatNParticles" )

pgun.MomentumRange.PdgCodes = [ 511 , -511 ]

tsvc = ToolSvc()
tsvc.addTool( EvtGenDecay , name = "EvtGenDecay" )
#tsvc.EvtGenDecay.UserDecayFile = "BDecayFile.dec"
#tsvc.EvtGenDecay.UserDecayFile = "/home/xuao/workdir/GaussDev_v49r17/mydecfile/Bd_D0rho0,KK=SSD_CP,DecProdCut_order.dec"
tsvc.EvtGenDecay.UserDecayFile = "/home/xuao/workdir/GaussDev_v49r17/mydecfile/Bd_D0rho0,KK=SSD_CP,DecProdCut_dcp.dec"
#tsvc.EvtGenDecay.UserDecayFile = "/home/xuao/workdir/GaussDev_v49r17/mydecfile/Bd_D0rho0,KK=SVS_CP,DecProdCut_tune.dec"
#tsvc.EvtGenDecay.UserDecayFile = "/home/xuao/workdir/GaussDev_v49r17/mydecfile/Bd_D0rho0,KK=SSD_CP,DecProdCut_tune.dec"
#tsvc.EvtGenDecay.UserDecayFile = "/home/xuao/workdir/GaussDev_v49r17/mydecfile/Bd_D0rho0,KK=SSD_CP,DecProdCut.dec"
#tsvc.EvtGenDecay.UserDecayFile = "/home/xuao/workdir/GaussDev_v49r17/mydecfile/Bd_D0rho0,KK=SVS_CP,DecProdCut.dec"
pgun.DecayTool = "EvtGenDecay"

pgun.MomentumRange.MomentumMin = 20.0*SystemOfUnits.GeV
pgun.MomentumRange.MomentumMax = 140.0*SystemOfUnits.GeV

from Configurables import LHCb__ParticlePropertySvc
LHCb__ParticlePropertySvc().Particles = ["KS0 16 310 0 1.865 410.1e-15 K_S0 310 0.00000000"]

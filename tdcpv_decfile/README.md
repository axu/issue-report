DecFiles intended to generate `B0 -> D~0 (-> K+ K-) rho0 (-> pi+ pi-)` decays with time-dependent CPV.
`SSD_CP` model is used.

The version with `D0` in the decay descriptor:
* Bd_D0rho0,KK=SSD_CP,DecProdCut.dec  
* BParticleGun.py    
* DaVinci-Job.py

The version with `KS0` in the decay descriptor and modify `KS0` to have mass and lifetime of `D0`:
* Bd_D0rho0,KK=SSD_CP,DecProdCut_dcp.dec	
* BParticleGun_dcp.py  
* DaVinci-Job_dcp.py

The command to run the DecFiles is `run.sh`.

The command to run the B0 -> Jpsi(-> mu mu) Ks(-> pi pi) DecFile is `run_b2jpsiks.sh`.

The issue: according to particle `ID`, `N(B0):N(B0bar)=2:1`, while we expect the ratio to be equal.

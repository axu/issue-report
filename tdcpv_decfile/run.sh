# configure lbenv
source /cvmfs/lhcb.cern.ch/lib/etc/cern_profile.sh

# set platform
lb-set-platform x86_64-slc6-gcc49-opt

# gauss env
../run bash --norc

# run gauss
gaudirun.py ./Gauss-Job.py $GAUSSOPTS/Gauss-2012.py $LBPGUNSROOT/options/PGuns.py ./BParticleGun.py $GAUSSOPTS/GenStandAlone.py $LBPYTHIA8ROOT/options/Pythia8.py
gaudirun.py ./Gauss-Job.py $GAUSSOPTS/Gauss-2012.py $LBPGUNSROOT/options/PGuns.py ./BParticleGun_dcp.py $GAUSSOPTS/GenStandAlone.py $LBPYTHIA8ROOT/options/Pythia8.py

We try to use the criteria `ReFitPVs=True && no BPV functor` to isolate the Turbo lines which
do not confiure the `ReFitPVs` correctly. More spesifically, if any of the algorithms used to build a Turbo line requires `ReFitPVs=True`
and it dose not contain `BPV` functors in the selection, we regard this Turbo line as mis-configured.
The results below show the output for one of the 2016 TCK. We do see some lines with large IPCHI2 cuts,
but most of them do not have th property IPCHI2>9.0.

To run:
```
lb-run moore/latest inspect_selection.py
```

# Hlt2CharmHadDpToKmKpKpTurbo
```
Hlt2CharmHadDpToKmKpKpTurboDpm2HHHFilter
{'ReFitPVs': True, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadSharedDetachedDpmChild_KFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
Hlt2CharmHadD2HHH_DpToKmKpKpCombiner
{'ReFitPVs': False, 'BPV': True, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0'], 'Decay': "[ '[D+ -> K- K+ K+]cc' ]"}
```


# Hlt2CharmHadDpToKmKpPipTurbo
```
Hlt2CharmHadDpToKmKpPipTurboDpm2HHHFilter
{'ReFitPVs': True, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadSharedDetachedDpmChild_piFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
Hlt2CharmHadD2HHH_DpToKmKpPipCombiner
{'ReFitPVs': False, 'BPV': True, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0'], 'Decay': "[ '[D+ -> K- K+ pi+]cc' ]"}
Hlt2CharmHadSharedDetachedDpmChild_KFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
```


# Hlt2CharmHadDpToKmPipPipTurbo
```
Hlt2CharmHadSharedDetachedDpmChild_piFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
Hlt2CharmHadD2HHH_DpToKmPipPipCombiner
{'ReFitPVs': False, 'BPV': True, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0'], 'Decay': "[ '[D+ -> K- pi+ pi+]cc' ]"}
Hlt2CharmHadDpToKmPipPipMassFilter
{'ReFitPVs': True, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadSharedDetachedDpmChild_KFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
```


# Hlt2CharmHadDpToKmPipPip_ForKPiAsymTurbo
```
Hlt2CharmHadDetAsym_DpToKmPipPipCombiner
{'ReFitPVs': False, 'BPV': True, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 25.', 'MIPCHI2DV(PRIMARY) > 25.', 'MIPCHI2DV(PRIMARY) > 25.', 'MIPCHI2DV(PRIMARY) > 25.', 'MIPCHI2DV(PRIMARY) > 25.', 'MIPCHI2DV(PRIMARY) > 25.'], 'Decay': "[ '[D+ -> K- pi+ pi+]cc' ]"}
Hlt2CharmHadSharedNoPIDDetachedChild_piFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 3.0'], 'BPV': False}
Hlt2CharmHadSharedNoPIDDetachedChild_KFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 3.0'], 'BPV': False}
Hlt2CharmHadDpToKmPipPip_ForKPiAsymTurboDpm2KPiPi_ForKPiAsymFilter
{'ReFitPVs': True, 'IPCHI2': '', 'BPV': False}
```


# Hlt2CharmHadDpToKmPipPip_LTUNBTurbo
```
Hlt2CharmHadSharedPromptChild_piFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadDpToKmPipPip_LTUNBTurboDpm2HHH_LTUNBFilter
{'ReFitPVs': True, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadSharedPromptChild_KFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadDpToKmPipPip_LTUNBTurboCombCombiner
{'ReFitPVs': False, 'BPV': True, 'IPCHI2': '', 'Decay': "[ '[D+ -> K- pi+ pi+]cc' ]"}
```


# Hlt2CharmHadDpToKpKpPimTurbo
```
Hlt2CharmHadSharedDetachedDpmChild_piFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
Hlt2CharmHadDpToKpKpPimTurboDpm2HHHFilter
{'ReFitPVs': True, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadSharedDetachedDpmChild_TightKFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
Hlt2CharmHadD2HHH_DpToKpKpPimCombiner
{'ReFitPVs': False, 'BPV': True, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0'], 'Decay': "[ '[D+ -> pi- K+ K+]cc' ]"}
```


# Hlt2CharmHadDpToKpPimPipTurbo
```
Hlt2CharmHadSharedDetachedDpmChild_piFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
Hlt2CharmHadD2HHH_DpToKpPimPipCombiner
{'ReFitPVs': False, 'BPV': True, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0'], 'Decay': "[ '[D+ -> K+ pi- pi+]cc' ]"}
Hlt2CharmHadDpToKpPimPipTurboDpm2HHHFilter
{'ReFitPVs': True, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadSharedDetachedDpmChild_KFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
```


# Hlt2CharmHadDpToPimPipPipTurbo
```
Hlt2CharmHadD2HHH_DpToPimPipPipCombiner
{'ReFitPVs': False, 'BPV': True, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0'], 'Decay': "[ '[D+ -> pi- pi+ pi+]cc' ]"}
Hlt2CharmHadDpToPimPipPipTurboDpm2HHHFilter
{'ReFitPVs': True, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadSharedDetachedDpmChild_piFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
```


# Hlt2CharmHadDspToKmKpKpTurbo
```
Hlt2CharmHadSharedDetachedDpmChild_KFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
Hlt2CharmHadD2HHH_DspToKmKpKpCombiner
{'ReFitPVs': False, 'BPV': True, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0'], 'Decay': "[ '[D_s+ -> K- K+ K+]cc' ]"}
Hlt2CharmHadDspToKmKpKpTurboDs2HHHFilter
{'ReFitPVs': True, 'IPCHI2': '', 'BPV': False}
```


# Hlt2CharmHadDspToKmKpPipTurbo
```
Hlt2CharmHadDspToKmKpPipMassFilter
{'ReFitPVs': True, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadD2HHH_DspToKmKpPipCombiner
{'ReFitPVs': False, 'BPV': True, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0'], 'Decay': "[ '[D_s+ -> K- K+ pi+]cc' ]"}
Hlt2CharmHadSharedDetachedDpmChild_piFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
Hlt2CharmHadSharedDetachedDpmChild_KFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
```


# Hlt2CharmHadDspToKmKpPip_LTUNBTurbo
```
Hlt2CharmHadSharedPromptChild_piFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadDspToKmKpPip_LTUNBTurboDs2HHH_LTUNBFilter
{'ReFitPVs': True, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadDspToKmKpPip_LTUNBTurboCombCombiner
{'ReFitPVs': False, 'BPV': True, 'IPCHI2': '', 'Decay': "[ '[D_s+ -> K- K+ pi+]cc' ]"}
Hlt2CharmHadSharedPromptChild_KFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
```


# Hlt2CharmHadDspToKmPipPipTurbo
```
Hlt2CharmHadSharedDetachedDpmChild_TightKFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
Hlt2CharmHadSharedDetachedDpmChild_piFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
Hlt2CharmHadDspToKmPipPipTurboDs2HHHFilter
{'ReFitPVs': True, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadD2HHH_DspToKmPipPipCombiner
{'ReFitPVs': False, 'BPV': True, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0'], 'Decay': "[ '[D_s+ -> K- pi+ pi+]cc' ]"}
```


# Hlt2CharmHadDspToKpKpPimTurbo
```
Hlt2CharmHadSharedDetachedDpmChild_piFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
Hlt2CharmHadDspToKpKpPimTurboDs2HHHFilter
{'ReFitPVs': True, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadD2HHH_DspToKpKpPimCombiner
{'ReFitPVs': False, 'BPV': True, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0'], 'Decay': "[ '[D_s+ -> K+ K+ pi-]cc' ]"}
Hlt2CharmHadSharedDetachedDpmChild_KFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
```


# Hlt2CharmHadDspToKpPimPipTurbo
```
Hlt2CharmHadSharedDetachedDpmChild_piFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
Hlt2CharmHadDspToKpPimPipTurboDs2HHHFilter
{'ReFitPVs': True, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadD2HHH_DspToKpPimPipCombiner
{'ReFitPVs': False, 'BPV': True, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0'], 'Decay': "[ '[D_s+ -> K+ pi- pi+]cc' ]"}
Hlt2CharmHadSharedDetachedDpmChild_KFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
```


# Hlt2CharmHadDspToPimPipPipTurbo
```
Hlt2CharmHadDspToPimPipPipTurboDs2HHHFilter
{'ReFitPVs': True, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadD2HHH_DspToPimPipPipCombiner
{'ReFitPVs': False, 'BPV': True, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0'], 'Decay': "[ '[D_s+ -> pi- pi+ pi+]cc' ]"}
Hlt2CharmHadSharedDetachedDpmChild_piFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
```


# Hlt2CharmHadDstp2D0Pip_D02KS0KS0_KS0DDTurbo
```
Hlt2CharmHadDstp2D0Pip_D02KS0KS0_KS0DDTurboD2KS0KS0_DDFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadSharedSoftTagChild_piFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
Hlt2SharedKsDDKsDD
{'ReFitPVs': False, 'BPV': True, 'IPCHI2': '', 'Decay': "[ 'KS0 -> pi+ pi-' ]"}
Hlt2CharmHadDstp2D0Pip_D02KS0KS0_KS0DDTurboCombCombiner
{'ReFitPVs': False, 'BPV': True, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'Decay': "[ 'D0 -> KS0 KS0' ]"}
Hlt2CharmHadSharedKsDDFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': True}
Hlt2CharmHadDstp2D0Pip_D02KS0KS0_KS0DDTurboD02V0V0_TAG_CPVCombiner
{'ReFitPVs': True, 'BPV': False, 'IPCHI2': '', 'Decay': "[ 'D*(2010)- -> D0 pi-' , 'D*(2010)+ -> D0 pi+' ]"}
```


# Hlt2CharmHadDstp2D0Pip_D02KS0KS0_KS0LLTurbo
```
Hlt2CharmHadSharedSoftTagChild_piFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
Hlt2SharedKsLLTFKsLLTF
{'ReFitPVs': False, 'BPV': True, 'IPCHI2': ["MIPCHI2DV(PRIMARY)>36)' ", "MIPCHI2DV(PRIMARY)>36)' "], 'Decay': "[ 'KS0 -> pi+ pi-' ]"}
Hlt2CharmHadSharedKsLLFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': True}
Hlt2CharmHadDstp2D0Pip_D02KS0KS0_KS0LLTurboD2KS0KS0_LLFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadDstp2D0Pip_D02KS0KS0_KS0LLTurboCombCombiner
{'ReFitPVs': False, 'BPV': True, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 9.0'], 'Decay': "[ 'D0 -> KS0 KS0' ]"}
Hlt2CharmHadDstp2D0Pip_D02KS0KS0_KS0LLTurboD02V0V0_TAG_CPVCombiner
{'ReFitPVs': True, 'BPV': False, 'IPCHI2': '', 'Decay': "[ 'D*(2010)- -> D0 pi-' , 'D*(2010)+ -> D0 pi+' ]"}
```


# Hlt2CharmHadDstp2D0Pip_D02KS0KS0_KS0LL_KS0DDTurbo
```
Hlt2CharmHadDstp2D0Pip_D02KS0KS0_KS0LL_KS0DDTurboD2KS0KS0_DDFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
Hlt2SharedKsDDKsDD
{'ReFitPVs': False, 'BPV': True, 'IPCHI2': '', 'Decay': "[ 'KS0 -> pi+ pi-' ]"}
Hlt2CharmHadDstp2D0Pip_D02KS0KS0_KS0LL_KS0DDTurboCombCombiner
{'ReFitPVs': False, 'BPV': True, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'Decay': "[ 'D0 -> KS0 KS0' ]"}
Hlt2CharmHadSharedKsLLFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': True}
Hlt2SharedKsLLTFKsLLTF
{'ReFitPVs': False, 'BPV': True, 'IPCHI2': ["MIPCHI2DV(PRIMARY)>36)' ", "MIPCHI2DV(PRIMARY)>36)' "], 'Decay': "[ 'KS0 -> pi+ pi-' ]"}
Hlt2CharmHadDstp2D0Pip_D02KS0KS0_KS0LL_KS0DDTurboD02V0V0_TAG_CPVCombiner
{'ReFitPVs': True, 'BPV': False, 'IPCHI2': '', 'Decay': "[ 'D*(2010)- -> D0 pi-' , 'D*(2010)+ -> D0 pi+' ]"}
Hlt2CharmHadSharedKsDDFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': True}
Hlt2CharmHadSharedSoftTagChild_piFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
```


# Hlt2CharmHadDstp2D0Pip_D02KmKpTurbo
```
Hlt2CharmHadDstp2D0Pip_D02KmKpCombiner
{'ReFitPVs': True, 'BPV': False, 'IPCHI2': '', 'Decay': "[ 'D*(2010)+ -> D0 pi+' , 'D*(2010)- -> D0 pi-' ]"}
Hlt2CharmHadD02HH_D0ToKmKpCombiner
{'ReFitPVs': True, 'BPV': True, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0'], 'Decay': "[ 'D0 -> K- K+' ]"}
Hlt2CharmHadSharedDetachedD0ToHHChildKFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 2.0'], 'BPV': False}
Hlt2CharmHadSharedSoftTagChild_piFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
```


# Hlt2CharmHadDstp2D0Pip_D02KmKp_LTUNBTurbo
```
Hlt2CharmHadD02HH_D0ToKmKp_LTUNBCombiner
{'ReFitPVs': True, 'BPV': True, 'IPCHI2': '', 'Decay': "[ 'D0 -> K- K+' ]"}
Hlt2CharmHadDstp2D0Pip_D02KmKp_LTUNBTurboD0_TAG_CPVCombiner
{'ReFitPVs': True, 'BPV': False, 'IPCHI2': '', 'Decay': "[ 'D*(2010)+ -> D0 pi+' , 'D*(2010)- -> D0 pi-' ]"}
Hlt2CharmHadSharedSoftTagChild_piFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadSharedPromptChild_KFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
```


# Hlt2CharmHadDstp2D0Pip_D02KmPipTurbo
```
Hlt2CharmHadD0_TAG_CPV_Dstp2D0Pip_D02KmPipCombiner
{'ReFitPVs': True, 'BPV': False, 'IPCHI2': '', 'Decay': "[ '[D*(2010)+ -> D0 pi+]cc' ]"}
Hlt2CharmHadD02HH_D0ToKmPipCombiner
{'ReFitPVs': True, 'BPV': True, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0'], 'Decay': "[ '[D0 -> K- pi+]cc' ]"}
Hlt2CharmHadSharedDetachedD0ToHHChildPiFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 2.0'], 'BPV': False}
Hlt2CharmHadSharedDetachedD0ToHHChildKFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 2.0'], 'BPV': False}
Hlt2CharmHadSharedSoftTagChild_piFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
```


# Hlt2CharmHadDstp2D0Pip_D02KmPip_LTUNBTurbo
```
Hlt2CharmHadDstp2D0Pip_D02KmPip_LTUNBTurboD0_TAG_CPVCombiner
{'ReFitPVs': True, 'BPV': False, 'IPCHI2': '', 'Decay': "[ '[D*(2010)+ -> D0 pi+]cc' ]"}
Hlt2CharmHadD02HH_D0ToKmPip_LTUNBCombiner
{'ReFitPVs': True, 'BPV': True, 'IPCHI2': '', 'Decay': "[ '[D0 -> K- pi+]cc' ]"}
Hlt2CharmHadSharedPromptChild_piFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadSharedSoftTagChild_piFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadSharedPromptChild_KFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
```


# Hlt2CharmHadDstp2D0Pip_D02KpPimTurbo
```
Hlt2CharmHadD02HH_D0ToKmPipCombiner
{'ReFitPVs': True, 'BPV': True, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0'], 'Decay': "[ '[D0 -> K- pi+]cc' ]"}
Hlt2CharmHadDstp2D0Pip_D02KpPimCombiner
{'ReFitPVs': True, 'BPV': False, 'IPCHI2': '', 'Decay': "[ '[D*(2010)- -> D0 pi-]cc' ]"}
Hlt2CharmHadSharedDetachedD0ToHHChildPiFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 2.0'], 'BPV': False}
Hlt2CharmHadSharedDetachedD0ToHHChildKFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 2.0'], 'BPV': False}
Hlt2CharmHadSharedSoftTagChild_piFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
```


# Hlt2CharmHadDstp2D0Pip_D02KpPim_LTUNBTurbo
```
Hlt2CharmHadSharedSoftTagChild_piFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadD02HH_D0ToKmPip_LTUNBCombiner
{'ReFitPVs': True, 'BPV': True, 'IPCHI2': '', 'Decay': "[ '[D0 -> K- pi+]cc' ]"}
Hlt2CharmHadSharedPromptChild_piFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadDstp2D0Pip_D02KpPim_LTUNBTurboD0_TAG_CPVCombiner
{'ReFitPVs': True, 'BPV': False, 'IPCHI2': '', 'Decay': "[ '[D*(2010)- -> D0 pi-]cc' ]"}
Hlt2CharmHadSharedPromptChild_KFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
```


# Hlt2CharmHadDstp2D0Pip_D02PimPipTurbo
```
Hlt2CharmHadSharedDetachedD0ToHHChildPiFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 2.0'], 'BPV': False}
Hlt2CharmHadDstp2D0Pip_D02PimPipCombiner
{'ReFitPVs': True, 'BPV': False, 'IPCHI2': '', 'Decay': "[ 'D*(2010)+ -> D0 pi+' , 'D*(2010)- -> D0 pi-' ]"}
Hlt2CharmHadD02HH_D0ToPimPipCombiner
{'ReFitPVs': True, 'BPV': True, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0'], 'Decay': "[ 'D0 -> pi- pi+' ]"}
Hlt2CharmHadSharedSoftTagChild_piFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
```


# Hlt2CharmHadDstp2D0Pip_D02PimPip_LTUNBTurbo
```
Hlt2CharmHadDstp2D0Pip_D02PimPip_LTUNBTurboD0_TAG_CPVCombiner
{'ReFitPVs': True, 'BPV': False, 'IPCHI2': '', 'Decay': "[ 'D*(2010)+ -> D0 pi+' , 'D*(2010)- -> D0 pi-' ]"}
Hlt2CharmHadSharedPromptChild_piFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadD02HH_D0ToPimPip_LTUNBCombiner
{'ReFitPVs': True, 'BPV': True, 'IPCHI2': '', 'Decay': "[ 'D0 -> pi- pi+' ]"}
Hlt2CharmHadSharedSoftTagChild_piFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
```


# Hlt2CharmHadLcpToPpKmKpTurbo
```
Hlt2CharmHadSharedDetachedLcChild_KFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
Hlt2CharmHadSharedDetachedLcChild_pFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
Hlt2CharmHadSharedDetachedLcChildTighterProtonFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadLcXic2HHH_LcpToKmPpKpCombiner
{'ReFitPVs': False, 'BPV': True, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0'], 'Decay': "[ '[Lambda_c+ -> K- p+ K+]cc' ]"}
Hlt2CharmHadLcpToPpKmKpTurboLc2HHHFilter
{'ReFitPVs': True, 'IPCHI2': '', 'BPV': False}
```


# Hlt2CharmHadLcpToPpKmPipTurbo
```
Hlt2CharmHadLc2HHH_LcpToKmPpPipMassFilter
{'ReFitPVs': True, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadSharedDetachedLcChild_KFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
Hlt2CharmHadSharedDetachedLcChildTighterProtonFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadSharedDetachedLcChild_piFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
Hlt2CharmHadLcXic2HHH_LcpToKmPpPipCombiner
{'ReFitPVs': False, 'BPV': True, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0'], 'Decay': "[ '[Lambda_c+ -> K- p+ pi+]cc' ]"}
Hlt2CharmHadSharedDetachedLcChild_pFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
```


# Hlt2CharmHadLcpToPpKmPip_LTUNBTurbo
```
Hlt2CharmHadSharedPromptChild_pFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadLcpToPpKmPip_LTUNBTurboCombCombiner
{'ReFitPVs': False, 'BPV': True, 'IPCHI2': '', 'Decay': "[ '[Lambda_c+ -> K- p+ pi+]cc' ]"}
Hlt2CharmHadSharedPromptChild_piFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadSharedPromptChildTighterProtonFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadLcpToPpKmPip_LTUNBTurboLc2HHH_LTUNBFilter
{'ReFitPVs': True, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadSharedPromptChild_KFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
```


# Hlt2CharmHadLcpToPpKpPimTurbo
```
Hlt2CharmHadSharedDetachedLcChild_KFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
Hlt2CharmHadLcpToPpKpPimTurboLc2HHHFilter
{'ReFitPVs': True, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadSharedDetachedLcChild_piFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
Hlt2CharmHadLcXic2HHH_LcpToPimPpKpCombiner
{'ReFitPVs': False, 'BPV': True, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0'], 'Decay': "[ '[Lambda_c+ -> pi- p+ K+]cc' ]"}
Hlt2CharmHadSharedDetachedLcChild_pFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
Hlt2CharmHadSharedDetachedLcChildTighterProtonFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
```


# Hlt2CharmHadLcpToPpPimPipTurbo
```
Hlt2CharmHadLcXic2HHH_LcpToPimPpPipCombiner
{'ReFitPVs': False, 'BPV': True, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0'], 'Decay': "[ '[Lambda_c+ -> pi- p+ pi+]cc' ]"}
Hlt2CharmHadSharedDetachedLcChildTighterProtonFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadSharedDetachedLcChild_piFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
Hlt2CharmHadSharedDetachedLcChild_pFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
Hlt2CharmHadLcpToPpPimPipTurboLc2HHHFilter
{'ReFitPVs': True, 'IPCHI2': '', 'BPV': False}
```


# Hlt2CharmHadXic0ToPpKmKmPipTurbo
```
Hlt2CharmHadSharedPromptChild_pFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadSharedPromptChild_piFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadSharedPromptChildTighterProtonFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadSharedPromptChild_KFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadXic0ToPpKmKmPipTurbo_DetachedHHHHCombinerCombiner
{'ReFitPVs': False, 'BPV': True, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0', 'MIPCHI2DV(PRIMARY) > 4.0'], 'Decay': "[ '[Xi_c0 -> p+ K- K- pi+]cc' ]"}
Hlt2CharmHadXic0ToPpKmKmPipTurboXic0ToPpKmKmPipTurboFilter
{'ReFitPVs': True, 'IPCHI2': '', 'BPV': False}
```


# Hlt2CharmHadXic0ToPpKmKmPip_LTUNBTurbo
```
Hlt2CharmHadSharedPromptChild_pFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadSharedPromptChild_piFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadXic0ToPpKmKmPip_LTUNBTurboXic0ToPpKmKmPip_LTUNBTurboCombiner
{'ReFitPVs': False, 'BPV': True, 'IPCHI2': '', 'Decay': "[ '[Xi_c0 -> p+ K- K- pi+]cc' ]"}
Hlt2CharmHadSharedPromptChildTighterProtonFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadXic0ToPpKmKmPip_LTUNBTurboXic0ToPpKmKmPip_LTUNBTurboFilter
{'ReFitPVs': True, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadSharedPromptChild_KFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
```


# Hlt2CharmHadXiccp2LcpKmPim_Lcp2PpKmPipTurbo
```
Hlt2CharmHadLc2HHH_LcpToKmPpPipMassFilter
{'ReFitPVs': True, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadSharedDetachedLcChild_KFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
Hlt2CharmHadSharedPromptChild_piFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadSharedDetachedLcChildTighterProtonFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadSharedDetachedLcChild_piFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
Hlt2CharmHadLcXic2HHH_LcpToKmPpPipCombiner
{'ReFitPVs': False, 'BPV': True, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0'], 'Decay': "[ '[Lambda_c+ -> K- p+ pi+]cc' ]"}
Hlt2CharmHadSharedDetachedLcChild_pFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
Hlt2CharmHadSharedPromptChild_KFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
```


# Hlt2CharmHadXiccp2LcpKmPip_Lcp2PpKmPipTurbo
```
Hlt2CharmHadLc2HHH_LcpToKmPpPipMassFilter
{'ReFitPVs': True, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadSharedDetachedLcChild_KFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
Hlt2CharmHadSharedPromptChild_piFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadSharedDetachedLcChildTighterProtonFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadSharedDetachedLcChild_piFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
Hlt2CharmHadLcXic2HHH_LcpToKmPpPipCombiner
{'ReFitPVs': False, 'BPV': True, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0'], 'Decay': "[ '[Lambda_c+ -> K- p+ pi+]cc' ]"}
Hlt2CharmHadSharedDetachedLcChild_pFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
Hlt2CharmHadSharedPromptChild_KFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
```


# Hlt2CharmHadXiccp2LcpKpPim_Lcp2PpKmPipTurbo
```
Hlt2CharmHadLc2HHH_LcpToKmPpPipMassFilter
{'ReFitPVs': True, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadSharedDetachedLcChild_KFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
Hlt2CharmHadSharedPromptChild_piFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadSharedDetachedLcChildTighterProtonFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadSharedDetachedLcChild_piFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
Hlt2CharmHadLcXic2HHH_LcpToKmPpPipCombiner
{'ReFitPVs': False, 'BPV': True, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0'], 'Decay': "[ '[Lambda_c+ -> K- p+ pi+]cc' ]"}
Hlt2CharmHadSharedDetachedLcChild_pFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
Hlt2CharmHadSharedPromptChild_KFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
```


# Hlt2CharmHadXiccp2XicpPimPim_Xicp2PpKmPipTurbo
```
Hlt2CharmHadSharedDetachedLcChild_KFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
Hlt2CharmHadSharedDetachedLcChildTighterProtonFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadSharedDetachedLcChild_piFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
Hlt2CharmHadLcXic2HHH_LcpToKmPpPipCombiner
{'ReFitPVs': False, 'BPV': True, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0'], 'Decay': "[ '[Lambda_c+ -> K- p+ pi+]cc' ]"}
Hlt2CharmHadXic2HHH_XicpToKmPpPipMassFilter
{'ReFitPVs': True, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadSharedDetachedLcChild_pFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
```


# Hlt2CharmHadXiccp2XicpPimPip_Xicp2PpKmPipTurbo
```
Hlt2CharmHadSharedDetachedLcChild_KFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
Hlt2CharmHadSharedDetachedLcChildTighterProtonFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadSharedDetachedLcChild_piFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
Hlt2CharmHadLcXic2HHH_LcpToKmPpPipCombiner
{'ReFitPVs': False, 'BPV': True, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0'], 'Decay': "[ '[Lambda_c+ -> K- p+ pi+]cc' ]"}
Hlt2CharmHadXic2HHH_XicpToKmPpPipMassFilter
{'ReFitPVs': True, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadSharedDetachedLcChild_pFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
```


# Hlt2CharmHadXiccpp2LcpKmPimPip_Lcp2PpKmPipTurbo
```
Hlt2CharmHadLc2HHH_LcpToKmPpPipMassFilter
{'ReFitPVs': True, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadSharedDetachedLcChild_KFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
Hlt2CharmHadSharedPromptChild_piFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadSharedDetachedLcChildTighterProtonFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadSharedDetachedLcChild_piFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
Hlt2CharmHadLcXic2HHH_LcpToKmPpPipCombiner
{'ReFitPVs': False, 'BPV': True, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0'], 'Decay': "[ '[Lambda_c+ -> K- p+ pi+]cc' ]"}
Hlt2CharmHadSharedDetachedLcChild_pFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
Hlt2CharmHadSharedPromptChild_KFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
```


# Hlt2CharmHadXiccpp2LcpKmPipPip_Lcp2PpKmPipTurbo
```
Hlt2CharmHadLc2HHH_LcpToKmPpPipMassFilter
{'ReFitPVs': True, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadSharedDetachedLcChild_KFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
Hlt2CharmHadSharedPromptChild_piFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadSharedDetachedLcChildTighterProtonFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadSharedDetachedLcChild_piFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
Hlt2CharmHadLcXic2HHH_LcpToKmPpPipCombiner
{'ReFitPVs': False, 'BPV': True, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0'], 'Decay': "[ '[Lambda_c+ -> K- p+ pi+]cc' ]"}
Hlt2CharmHadSharedDetachedLcChild_pFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
Hlt2CharmHadSharedPromptChild_KFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
```


# Hlt2CharmHadXiccpp2LcpKpPimPip_Lcp2PpKmPipTurbo
```
Hlt2CharmHadLc2HHH_LcpToKmPpPipMassFilter
{'ReFitPVs': True, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadSharedDetachedLcChild_KFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
Hlt2CharmHadSharedPromptChild_piFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadSharedDetachedLcChildTighterProtonFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadSharedDetachedLcChild_piFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
Hlt2CharmHadLcXic2HHH_LcpToKmPpPipCombiner
{'ReFitPVs': False, 'BPV': True, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0'], 'Decay': "[ '[Lambda_c+ -> K- p+ pi+]cc' ]"}
Hlt2CharmHadSharedDetachedLcChild_pFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
Hlt2CharmHadSharedPromptChild_KFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
```


# Hlt2CharmHadXiccpp2XicpPim_Xicp2PpKmPipTurbo
```
Hlt2CharmHadSharedDetachedLcChild_KFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
Hlt2CharmHadSharedDetachedLcChildTighterProtonFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadSharedDetachedLcChild_piFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
Hlt2CharmHadLcXic2HHH_LcpToKmPpPipCombiner
{'ReFitPVs': False, 'BPV': True, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0'], 'Decay': "[ '[Lambda_c+ -> K- p+ pi+]cc' ]"}
Hlt2CharmHadXic2HHH_XicpToKmPpPipMassFilter
{'ReFitPVs': True, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadSharedDetachedLcChild_pFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
```


# Hlt2CharmHadXiccpp2XicpPip_Xicp2PpKmPipTurbo
```
Hlt2CharmHadSharedDetachedLcChild_KFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
Hlt2CharmHadSharedDetachedLcChildTighterProtonFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadSharedDetachedLcChild_piFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
Hlt2CharmHadLcXic2HHH_LcpToKmPpPipCombiner
{'ReFitPVs': False, 'BPV': True, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0'], 'Decay': "[ '[Lambda_c+ -> K- p+ pi+]cc' ]"}
Hlt2CharmHadXic2HHH_XicpToKmPpPipMassFilter
{'ReFitPVs': True, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadSharedDetachedLcChild_pFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
```


# Hlt2CharmHadXicpToPpKmPipTurbo
```
Hlt2CharmHadSharedDetachedLcChild_KFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
Hlt2CharmHadSharedDetachedLcChildTighterProtonFilter
{'ReFitPVs': False, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadSharedDetachedLcChild_piFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
Hlt2CharmHadLcXic2HHH_LcpToKmPpPipCombiner
{'ReFitPVs': False, 'BPV': True, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0', 'MIPCHI2DV(PRIMARY) > 6.0'], 'Decay': "[ '[Lambda_c+ -> K- p+ pi+]cc' ]"}
Hlt2CharmHadXic2HHH_XicpToKmPpPipMassFilter
{'ReFitPVs': True, 'IPCHI2': '', 'BPV': False}
Hlt2CharmHadSharedDetachedLcChild_pFilter
{'ReFitPVs': False, 'IPCHI2': ['MIPCHI2DV(PRIMARY) > 4.0'], 'BPV': False}
```



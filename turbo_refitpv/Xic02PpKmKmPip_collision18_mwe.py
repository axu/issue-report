"""
Generate ntuple with ReFitPV enabled
"""
from Configurables import DaVinci
DaVinci().EvtMax =  -1
DaVinci().SkipEvents = 0
DaVinci().DataType = "2018"
DaVinci().Simulation = False
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().TupleFile = "Tuple_debug.root"
DaVinci().Lumi = True
DaVinci().InputType = "MDST"
DaVinci().RootInTES = '/Event/Charmmultibody/Turbo'
DaVinci().Turbo = True
DaVinci().TurboStream = 'Charmmultibody'

from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *

Xic02PpKmKmPip_Branch = {
    "Xic0"  :  "^([ Xi_c0 -> p+ K- K- pi+ ]CC)",
    "Pp"    :  "[ Xi_c0 -> ^p+ K- K- pi+ ]CC",
    "Km1"   :  "[ Xi_c0 -> p+ ^K- K- pi+ ]CC",
    "Km2"   :  "[ Xi_c0 -> p+ K- ^K- pi+ ]CC",
    "Pip"   :  "[ Xi_c0 -> p+ K- K- ^pi+ ]CC"
}

line = "Hlt2CharmHadXic0ToPpKmKmPipTurbo"

from PhysSelPython.Wrappers import (TurboData,
        SelectionSequence,
        ValidBPVSelection,
        TupleSelection,
        PassThroughSelection,
)
from Configurables import EventNodeKiller

# for datatype<=2016
#pvsLocation = 'Primary'
# for datatype>2016
pvsLocation = 'Rec/Vertex/Primary'

data = TurboData(line,
        OutputLevel = 1)

killer = EventNodeKiller('P2PVKillerAlg_{}'.format(line),
        Nodes = ['/'.join([DaVinci().getRootInTES(),
            data.outputLocation().replace('/Particles', '/Particle2VertexRelations')])],
        OutputLevel = 1)
print('EventNodeKiller Nodes: {}'.format(killer.Nodes))
killersel = PassThroughSelection('P2PVKiller_{}'.format(line),
        RequiredSelection = data,
        Algorithm = killer)
refitpvsel = ValidBPVSelection([killersel],
        ReFitPVs = True,
        InputPrimaryVertices = pvsLocation,
        OutputLevel = 1)
print('ValidBPVSelection outputLocation: {}'.format(refitpvsel.outputLocation()))

dttsel = TupleSelection('Xic02PpKmKmPip_Tuple',
        [refitpvsel],
        Decay = "[ Xi_c0 -> ^p+ ^K- ^K- ^pi+ ]CC",
        Branches = Xic02PpKmKmPip_Branch,
        ToolList = ['TupleToolPrimaries',
                    'TupleToolGeometry',
                    'TupleToolPid',
                    'TupleToolANNPID',
                    'TupleToolRecoStats',
                    'TupleToolKinematic',
                    'TupleToolEventInfo',
                    'TupleToolTrackInfo'],
        P2PVInputLocations = [refitpvsel.outputLocation().replace('/Particles', '/Particle2VertexRelations')]
)
dtt = dttsel.algorithm()
dtt.ReFitPVs = True

from PhysConf.Selections import SelectionSequence
dtt_seq = SelectionSequence('{}_Seq'.format(line), TopSelection = dttsel).sequence()

DaVinci().UserAlgorithms.append( dtt_seq )

from PhysConf.Filters import LoKi_Filters
trigger_filter = LoKi_Filters(
    HLT2_Code="HLT_PASS_RE('.*{0}.*')".format(line)
)

DaVinci().EventPreFilters = trigger_filter.filters('TriggerFilter')

from Configurables import CondDB
CondDB ( LatestGlobalTagByDataType = '2018')

TEST = True
if TEST:
    # Local test only
    DaVinci().EvtMax = 20000
    DaVinci().PrintFreq = 100
    from GaudiConf import IOHelper
    IOHelper().inputFiles([
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision18/CHARMMULTIBODY.MDST/00080040/0000/00080040_00004442_1.charmmultibody.mdst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision18/CHARMMULTIBODY.MDST/00076507/0000/00076507_00002163_1.charmmultibody.mdst',
    ], clear=True)

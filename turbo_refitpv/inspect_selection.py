import sys
import re

from TCKUtils.utils import getProperties, getTCKs, getHlt2Lines

def describe(tck, name, props, enable_refit, indentation):
    """ Recurse down into this algorithm's properties,
        inspect the 'ReFit' property,
        and do this recursively for its inputs """
    indent = indentation*' '

    def iprint(s):
        print('{}{}'.format(indent, s))

    def get_ipchi_cut(s):
        positions = [m.start() for m in re.finditer('IP', s)]
        #position = s.find('MIPCHI2DV(PRIMARY)')
        return [ s[p-1:p+23] for p in positions ]
    
    if 'Particle' in props:
        # Have a Particle maker, no need to descend further
        #iprint(props['Output'])
        return

    header_name = '\n{0}{1}\n{0}{2}'.format(indent, name, len(name)*'=')
    #print(header_name)

    header_sel = '\n{0}Selection\n{0}---------'.format(indent)
    #print(header_sel)

    # print ReFitPVs property
    #iprint('ReFitPVs: {}'.format(props['ReFitPVs']))

    if 'Code' in props:
        # We have a FilterDesktop
        #iprint('Code: {}'.format(props['Code']))
        if props['ReFitPVs']=='True' or enable_refit.get('Save',False):
            enable_refit[name] = {}
            enable_refit[name]['ReFitPVs'] = True if props['ReFitPVs']=='True' else False
            enable_refit[name]['BPV'] = 'BPV' in props['Code']
            enable_refit[name]['IPCHI2'] = get_ipchi_cut(props['Code']) if 'MIPCHI2DV(PRIMARY)' in props['Code'] else ''
    elif 'DaughtersCuts' in props:
        # We have a CombineParticles
        preambulo = eval(props['Preambulo'])
        #iprint('DecayDescriptors: {}'.format(props['DecayDescriptors']))
        if preambulo:
            #iprint('Preambulo:        {}'.format(preambulo))
            pass
        #iprint('DaughtersCuts:    {}'.format(props['DaughtersCuts']))
        #iprint('CombinationCut:   {}'.format(props['CombinationCut']))
        #iprint('MotherCut:        {}'.format(props['MotherCut']))
        if props['ReFitPVs']=='True' or enable_refit.get('Save',False):
            enable_refit[name] = {}
            enable_refit[name]['Decay'] = props['DecayDescriptors']
            enable_refit[name]['ReFitPVs'] = True if props['ReFitPVs']=='True' else False
            enable_refit[name]['BPV'] = 'BPV' in props['MotherCut']
            enable_refit[name]['IPCHI2'] = get_ipchi_cut(props['DaughtersCuts']) if 'MIPCHI2DV(PRIMARY)' in props['DaughtersCuts'] else ''
    elif 'TisTosSpecs' in props:
        # We have a TisTosTagger
        #iprint('TisTosSpecs:  {}'.format(props['TisTosSpecs']))
        pass
    else:
        assert False, 'Do not know the type of {}'.format(props)

    header_inputs = '\n{0}Inputs\n{0}------'.format(indent)
    #print(header_inputs)

    inputs = eval(props['Inputs'])
    for input_name in inputs:
        assert input_name.startswith('Hlt2/')
        input_name = '.*/{}$'.format(input_name[len('Hlt2/'):])
        input_props = getProperties(tck, input_name)
        #assert len(input_props) == 1
        if not len(input_props) == 1:
            #print(input_name)
            continue
        input_name, input_props = getProperties(tck, input_name).items()[0]
        if props['ReFitPVs']=='True' or enable_refit.get('Save',False):
            enable_refit['Save'] = True
            if props['ReFitPVs']=='True':
                enable_refit['Correct'] = enable_refit[name].get('BPV',False)
        describe(tck, input_name, input_props, enable_refit, indentation + 4)

def selections_78(tck, line):
    assert line.startswith('Hlt2') and line.endswith('Turbo')

    mover_name = '^FilterDesktop/{}Mover$'.format(line)
    mover_props = getProperties(tck, mover_name).values()[0]
    mover_inputs = eval(mover_props['Inputs'])
    assert len(mover_inputs) == 1

    filter_name = mover_inputs[0]
    assert filter_name.startswith('Hlt2/')
    filter_name = '.*/{}$'.format(filter_name[len('Hlt2/'):])
    filter_props = getProperties(tck, filter_name)
    assert len(filter_props) == 1
    filter_name, filter_props = filter_props.items()[0]

    refitpvs = {}
    describe(tck, filter_name, filter_props, refitpvs, indentation=0)
    return refitpvs

def selections_6(tck, line):
    assert line.startswith('Hlt2') and line.endswith('Turbo')

    decision_name = '^HltCopySelection<LHCb::Particle>/{}Decision$'.format(line)
    decision_props = getProperties(tck, decision_name).values()[0]
    decision_input = eval(decision_props['InputSelection'])
    decision_input_alg, = re.match('TES:/Event/Hlt2/(.*)/Particles', decision_input).groups()

    filter_name = '.*/{}$'.format(decision_input_alg)
    filter_props = getProperties(tck, filter_name)
    assert len(filter_props) == 1
    filter_name, filter_props = filter_props.items()[0]

    refitpvs = {}
    describe(tck, filter_name, filter_props, refitpvs, indentation=0)
    return refitpvs


def main():
    #tcks = [ int(hex(559290119),16), int(hex(560011017),16) ]
    #tcks = [ 0x21381609,0x21561707,0x21751801 ]
    #tcks = [ 0x21381609 ]
    #lines = [
    #   'Hlt2CharmHadD02KmPipTurbo',
    #   'Hlt2CharmHadDstp2D0Pip_D02KmKp_LTUNBTurbo',
    #   'Hlt2CharmHadLcpToPpKmPipTurbo',
    #   'Hlt2CharmHadXiccp2LcpKmPip_Lcp2PpKmPipTurbo',
    #   'Hlt2CharmHadXicpToPpKmPipTurbo',
    #   'Hlt2CharmHadXiccpp2LcpKmPipPip_Lcp2PpKmPipTurbo',
    #]
    tcks_16 = [int(k, 16) for k, v in getTCKs() if k.startswith('0x2') and k[-4:-2] == '16' and 'Physics' in v]
    lines = [ l for l in getHlt2Lines(tcks_16[0]) if l.endswith('Turbo') ]
    tck = tcks_16[0]
    for line in lines:
        if '16'==hex(tck)[-4:-2]:
            test = selections_6(tck, line)
            if test.get('Save',False) and not test.get('Correct',False):
                print('# '+line)
                print('```')
                for k,v in test.items():
                    if k=='Save' or k=='Correct':
                        continue
                    print(k)
                    print(v)
                print('```')
                print('\n')
        elif '17'==hex(tck)[-4:-2] or '18'==hex(tck)[-4:-2]:
            selections_78(tck, line)
        else:
            print('Unknown TCK types!')


if __name__ == '__main__':
    main()


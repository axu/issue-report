
Hlt2CharmHadDpToPimPipPipTurboDpm2HHHFilter
===========================================

Selection
---------
ReFitPVs: True
Code: in_range( 1789.0 , M , 1949.0 )

Inputs
------

    Hlt2CharmHadD2HHH_DpToPimPipPipTisTosTagger
    ===========================================

    Selection
    ---------
    ReFitPVs: False
    TisTosSpecs:  { 'Hlt1.*Track.*Decision%TOS' : 0 }

    Inputs
    ------

        Hlt2CharmHadD2HHH_DpToPimPipPipCombiner
        =======================================

        Selection
        ---------
        ReFitPVs: False
        DecayDescriptors: [ '[D+ -> pi- pi+ pi+]cc' ]
        Preambulo:        ['import math', 'lcldira = math.cos( 0.01 )']
        DaughtersCuts:    { '' : 'ALL' , 'K+' : '(PT > 250.0) & (MIPCHI2DV(PRIMARY) > 4.0)' , 'mu+' : '(PT > 250.0) & (MIPCHI2DV(PRIMARY) > 4.0)' , 'p+' : '(PT > 250.0) & (MIPCHI2DV(PRIMARY) > 4.0)' , 'pi+' : '(PT > 250.0) & (MIPCHI2DV(PRIMARY) > 4.0)' , 'pi-' : '(PT > 250.0) & (MIPCHI2DV(PRIMARY) > 4.0)' }
        CombinationCut:   (in_range( 1779.0, AM, 1959.0 )) & ((APT1+APT2+APT3) > 3200.0 ) & (AHASCHILD(PT > 1000.0)) & (ANUM(PT > 400.0) >= 2) & (AHASCHILD((MIPCHI2DV(PRIMARY)) > 50.0)) & (ANUM(MIPCHI2DV(PRIMARY) > 10.0) >= 2)
        MotherCut:        (VFASPF(VCHI2PDOF) < 6) & (BPVDIRA > lcldira ) & (BPVLTIME() > 0.0004 )

        Inputs
        ------

            Hlt2CharmHadSharedDetachedDpmChild_TightpiFilter
            ================================================

            Selection
            ---------
            ReFitPVs: False
            Code:   (TRCHI2DOF < 3.0 )& (PT > 200.0)& (P > 1000.0)& (MIPCHI2DV(PRIMARY) > 4.0)& (PIDK < 1)

            Inputs
            ------

                Hlt2CharmHadSharedDetachedDpmChild_piFilter
                ===========================================

                Selection
                ---------
                ReFitPVs: False
                Code:   (TRCHI2DOF < 3.0 )& (PT > 200.0)& (P > 1000.0)& (MIPCHI2DV(PRIMARY) > 4.0)& (PIDK < 5)

                Inputs
                ------
                    ReFitPVs: False
                    Hlt2/Hlt2BiKalmanFittedRichPions/Particles

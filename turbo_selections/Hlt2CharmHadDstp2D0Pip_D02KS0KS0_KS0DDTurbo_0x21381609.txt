
Hlt2CharmHadDstp2D0Pip_D02KS0KS0_KS0DDTurboD02V0V0_TAG_CPVCombiner
==================================================================

Selection
---------
ReFitPVs: True
DecayDescriptors: [ 'D*(2010)- -> D0 pi-' , 'D*(2010)+ -> D0 pi+' ]
DaughtersCuts:    { '' : 'ALL' , 'D0' : 'ALL' , 'D~0' : 'ALL' , 'pi+' : 'ALL' , 'pi-' : 'ALL' }
CombinationCut:   in_range( -9.57018, (AM - AM1 - AM2), 25.42982 )
MotherCut:        (VFASPF(VCHI2PDOF) < 25.0)& in_range( -9.57018, (M - M1 - M2), 20.42982 )

Inputs
------

    Hlt2CharmHadDstp2D0Pip_D02KS0KS0_KS0DDTurboD2KS0KS0_DDFilter
    ============================================================

    Selection
    ---------
    ReFitPVs: False
    Code: in_range( 1789.0 , M , 1949.0 )

    Inputs
    ------

        Hlt2CharmHadDstp2D0Pip_D02KS0KS0_KS0DDTurboCombTisTosTagger
        ===========================================================

        Selection
        ---------
        ReFitPVs: False
        TisTosSpecs:  { 'Hlt1.*Track.*Decision%TOS' : 0 }

        Inputs
        ------

            Hlt2CharmHadDstp2D0Pip_D02KS0KS0_KS0DDTurboCombCombiner
            =======================================================

            Selection
            ---------
            ReFitPVs: False
            DecayDescriptors: [ 'D0 -> KS0 KS0' ]
            Preambulo:        ['import math', 'lcldira = math.cos( 0.0346 )']
            DaughtersCuts:    { '' : 'ALL' , 'KS0' : '(PT > 750.0) &(MIPCHI2DV(PRIMARY) > 4.0)' }
            CombinationCut:   (in_range( 1779.0, AM, 1959.0 )) & ((APT1+APT2) > 2000.0 )
            MotherCut:        (VFASPF(VCHI2PDOF) < 10.0) & (BPVDIRA > lcldira ) & (BPVVDCHI2 > 10.0 ) & (BPVLTIME() > 0.0002 )

            Inputs
            ------

                Hlt2CharmHadSharedKsDDFilter
                ============================

                Selection
                ---------
                ReFitPVs: False
                Code:  (BPVLTIME() > 0.0005) & (in_range( 300.0, VFASPF(VZ), 2275.0 ))

                Inputs
                ------

                    Hlt2SharedKsDDKsDD
                    ==================

                    Selection
                    ---------
                    ReFitPVs: False
                    DecayDescriptors: [ 'KS0 -> pi+ pi-' ]
                    DaughtersCuts:    { '' : 'ALL' , 'pi+' : '(TRCHI2DOF<4) & (P>3000*MeV) & (PT > 175.*MeV)' , 'pi-' : '(TRCHI2DOF<4) & (P>3000*MeV) & (PT > 175.*MeV)' }
                    CombinationCut:   (ADAMASS('KS0')<80*MeV)
                    MotherCut:        (ADMASS('KS0')<64*MeV) & (VFASPF(VCHI2PDOF)<30)  & (BPVVDZ > 400.0*mm)

                    Inputs
                    ------
                        ReFitPVs: False
                        Hlt2/Hlt2BiKalmanFittedDownPions/Particles

    Hlt2CharmHadSharedSoftTagChild_piFilter
    =======================================

    Selection
    ---------
    ReFitPVs: False
    Code:   (TRCHI2DOF < 3.0 )& (PT > 100.0)& (P > 1000.0)

    Inputs
    ------
        ReFitPVs: False
        Hlt2/Hlt2BiKalmanFittedPions/Particles

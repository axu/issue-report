
Hlt2CharmHadXiccpp2XicpPim_Xicp2PpKmPipTurboCombTisTosTagger
============================================================

Selection
---------
ReFitPVs: False
TisTosSpecs:  { 'Hlt1.*Track.*Decision%TOS' : 0 }

Inputs
------

    Hlt2CharmHadXiccpp2XicpPim_Xicp2PpKmPipTurboCombCombiner
    ========================================================

    Selection
    ---------
    ReFitPVs: False
    DecayDescriptors: [ '[Xi_cc++ -> Xi_c+ pi-]cc' ]
    Preambulo:        ['import math', 'lcldira = math.cos( 1.57079632679 )']
    DaughtersCuts:    { '' : 'ALL' , 'K+' : '(MIPCHI2DV(PRIMARY) > -1.0)' , 'KS0' : 'ALL' , 'Lambda0' : 'ALL' , 'Xi_c+' : 'ALL' , 'Xi_c~-' : 'ALL' , 'pi+' : '(MIPCHI2DV(PRIMARY) > -1.0)' , 'pi-' : '(MIPCHI2DV(PRIMARY) > -1.0)' }
    CombinationCut:   (in_range( 3100.0, AM, 4000.0 )) & ((APT1+APT2) > 2000.0 )
    MotherCut:        (VFASPF(VCHI2PDOF) < 20.0) & (BPVDIRA > lcldira ) & (BPVVDCHI2 > -1.0 ) & (BPVLTIME() > -1.0 )

    Inputs
    ------

        Hlt2CharmHadSharedPromptChild_piFilter
        ======================================

        Selection
        ---------
        ReFitPVs: False
        Code:   (TRCHI2DOF < 3.0 )& (PT > 500.0)& (P > 1000.0)& (PIDK < 0)

        Inputs
        ------
            ReFitPVs: False
            Hlt2/Hlt2BiKalmanFittedRichPions/Particles

        Hlt2CharmHadXic2HHH_XicpToKmPpPipMassFilter
        ===========================================

        Selection
        ---------
        ReFitPVs: True
        Code: in_range( 2392.0 , M , 2543.0 )

        Inputs
        ------

            Hlt2CharmHadLcXic2HHH_LcpToKmPpPipTisTosTagger
            ==============================================

            Selection
            ---------
            ReFitPVs: False
            TisTosSpecs:  { 'Hlt1.*Track.*Decision%TOS' : 0 }

            Inputs
            ------

                Hlt2CharmHadLcXic2HHH_LcpToKmPpPipCombiner
                ==========================================

                Selection
                ---------
                ReFitPVs: False
                DecayDescriptors: [ '[Lambda_c+ -> K- p+ pi+]cc' ]
                Preambulo:        ['import math', 'lcldira = math.cos( 0.01 )']
                DaughtersCuts:    { '' : 'ALL' , 'K+' : '(PT > 200.0) & (MIPCHI2DV(PRIMARY) > 6.0)' , 'K-' : '(PT > 200.0) & (MIPCHI2DV(PRIMARY) > 6.0)' , 'mu+' : '(PT > 200.0) & (MIPCHI2DV(PRIMARY) > 6.0)' , 'p+' : '(PT > 200.0) & (MIPCHI2DV(PRIMARY) > 6.0)' , 'pi+' : '(PT > 200.0) & (MIPCHI2DV(PRIMARY) > 6.0)' , 'pi-' : '(PT > 200.0) & (MIPCHI2DV(PRIMARY) > 6.0)' , 'p~-' : '(PT > 200.0) & (MIPCHI2DV(PRIMARY) > 6.0)' }
                CombinationCut:   (in_range( 2201.0, AM, 2553.0 )) & ((APT1+APT2+APT3) > 3000.0 ) & (AHASCHILD(PT > 1000.0)) & (ANUM(PT > 400.0) >= 2) & (AHASCHILD((MIPCHI2DV(PRIMARY)) > 16.0)) & (ANUM(MIPCHI2DV(PRIMARY) > 9.0) >= 2)
                MotherCut:        (VFASPF(VCHI2PDOF) < 10.0) & (BPVDIRA > lcldira ) & (BPVLTIME() > 0.00015 )

                Inputs
                ------

                    Hlt2CharmHadSharedDetachedLcChildTighterProtonFilter
                    ====================================================

                    Selection
                    ---------
                    ReFitPVs: False
                    Code: (PIDp > 5.0  ) & ( (PIDp-PIDK) > 5.0 ) & ( P > 10000.0 )

                    Inputs
                    ------

                        Hlt2CharmHadSharedDetachedLcChild_pFilter
                        =========================================

                        Selection
                        ---------
                        ReFitPVs: False
                        Code:   (TRCHI2DOF < 3.0 )& (PT > 200.0)& (P > 1000.0)& (MIPCHI2DV(PRIMARY) > 4.0)& (PIDp > 5)

                        Inputs
                        ------
                            ReFitPVs: False
                            Hlt2/Hlt2BiKalmanFittedRichProtons/Particles

                    Hlt2CharmHadSharedDetachedLcChild_KFilter
                    =========================================

                    Selection
                    ---------
                    ReFitPVs: False
                    Code:   (TRCHI2DOF < 3.0 )& (PT > 200.0)& (P > 1000.0)& (MIPCHI2DV(PRIMARY) > 4.0)& (PIDK > 5)

                    Inputs
                    ------
                        ReFitPVs: False
                        Hlt2/Hlt2BiKalmanFittedRichKaons/Particles

                    Hlt2CharmHadSharedDetachedLcChild_piFilter
                    ==========================================

                    Selection
                    ---------
                    ReFitPVs: False
                    Code:   (TRCHI2DOF < 3.0 )& (PT > 200.0)& (P > 1000.0)& (MIPCHI2DV(PRIMARY) > 4.0)& (PIDK < 5)

                    Inputs
                    ------
                        ReFitPVs: False
                        Hlt2/Hlt2BiKalmanFittedRichPions/Particles

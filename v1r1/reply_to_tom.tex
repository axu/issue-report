\section{Replies to Tom}%
\label{sec:replies_to_tom}

\subsection{General comments}%
\label{sub:general_comments}

\begin{description}
  \item[- Would it be possible to add the numerical fit results 
    for some of the important fits? E.g. Figure 69 and Figure 72. ]

    Now the fitted mass, $R$ are already shown. Please let us know
    which fit parameter(s) you are interested. 

  \item[- In Ao’s reply to the mailing list on 28th May 
    you mentioned using a fit range that extends up to 3800 MeV 
    such that a limit could also be place on Omega\_cc+ decaying to the same final state.
    I was wondering whether you planned on including this in the same analysis? 
    If so, what extra work would be needed for this? Does it require
    dedicated MC?]

    Yes, we plan to add this information. Since the only difference of 
    $\Omega_{cc}^{+}\to\Lc\Km\pip$ from the \Xiccp mode is
    the mass of the mother particle, we can re-weight the \Xiccp MC to evaluate the efficiency,
    as has been done when we evaluate the efficiency variation with mass for \Xiccp mode.
    Dedicated MC samples are not necessary.
    In fact, from Sec.~5.6 in note v1r1 we can see that the efficiency variation with mass is small
    in a wide range (The prediction of $\Omega_{cc}^{+}$ mass is about 3738\mev). 
    So essentially we can interpret the upper limits also as those of $\Omega_{cc}^{+}$ mode.

  \item[- I was wondering what the timeline is for the next steps? e.g. producing a paper draft etc.]

    Yes, we will try to produce a paper draft as soon as possible.

\end{description}


\subsection{Specific comments}%
\label{sub:specific_comments}


\subsubsection{On global significance}%
\label{ssub:on_global_significance}

In note v1r1, the distributions of the toy experiments (Fig~65 and Fig~123) show 
larger fluctuations than statistical uncertainties. 
This indicates that there may be some correlations between simulated events.
We have investigated further and found that the culprit is 
the misconfiguration of the seed of the random generator.

Random generator is used in two cases in the toy experiment: 
1) generate the number of events, which follows the Poisson distribution, in each toy experiment;
2) generate the mass spectrum in each toy experiment.
To speed up the generation of toy experiments,
we use multiprocess locally and Ganga on the grid.
To avoid correlation between subjobs, the random seed need to be set differently for each subjob.
This was only done for 1) with \texttt{gRandom->SetSeed(seed)}.
The configuration for 2) with \texttt{RooRandom::randomGenerator()->SetSeed(seed)} was missing,
which leads to correlations between subjobs.

The corrected plots corresponding to Figure~65 in note v1r1 is shown in Fig~\ref{fig:t0}
(for illustration purpose, only $t_0>0$ is shown in the plots).
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{figs/t0_local.pdf}
  \includegraphics[width=0.45\linewidth]{figs/t0_global.pdf}
  \caption{Corrected plots corresponding to Figure~65 in note v1r1.}
  \label{fig:t0}
\end{figure}
One can see that the distribution is smooth and the fluctuation has statistical nature.
In note v1r1, we use 
\begin{equation}
  \frac{1}{2}\delta(t_0) + \frac{1}{2} \frac{t_0^{\gamma-1} e ^{-t_0/\beta}}{\beta^{\gamma}\Gamma(\gamma)}
\end{equation}
to describe the data and calculate the integral.
However, with the correct configuration and large toy sample size,
the above model is not able to describe the tail very well, as shown in the left plot of Fig~\ref{fig:t0_fit}.
One choice is to fit only the tail of the distribution as shown in the right plot of Fig~\ref{fig:t0_fit},
which has been used in LHCb-ANA-2018-043 Figure 58.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{figs/pglobal_Run12_1step.pdf}
  \includegraphics[width=0.45\linewidth]{figs/pglobal_range.pdf}
  \caption{The fit to the toy distributions.}
  \label{fig:t0_fit}
\end{figure}

Alternatively, as Lars suggested, we use the equation
\begin{equation}
  \label{eq:bound}
  P(t(\hat{m})>c) \leq P(\chi^2_s > c) + \langle N(c_0) \rangle \left( \frac{c}{c_0} \right) ^{(s-1)/2} e ^{-(c-c_0)/2}
\end{equation}
in Ref.~\cite{Gross:2010qma} to evaluate the tail probability (which is the $p$-value).
The $c$ is taken to be the observed $t_0$ in data, $c_0=0.5$ and $s=1$.
$\langle N(c_0) \rangle$, the so-called expected number of ``upcrossing'',
is evaluated with a small number (1K) of background-only toy experiments.
As validation, we can also calculate the $p$-value by counting the events in the toy distributions.
The comparison of toy experiments and Eq.~\ref{eq:bound} is shown in Fig.~\ref{fig:t0_tail},
which shows that Eq.~\ref{eq:bound} gives decent approximation for large $c$
and saves a lot of CPU time.
The updated note shows results with this method,
which is fast, with good accuracy and easy to extend to multi-channels.
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.6\linewidth]{figs/tail_prob.pdf}
  \caption{The comparison of (blue solid line) toy experiments and 
  (red dotted line) Eq.~\ref{eq:bound}.}
  \label{fig:t0_tail}
\end{figure}

For the choice of interested range in evaluating the global significance,
we think it does not have to be the same as the fit range.
Once the range is determined, the evaluation procedure can 
give us the reasonable LEE correction under this range.
As you mentioned, 3500--3700 was motivated by theoretical predictions.
But of course, the wider the range is,
the more reduction of the local significance will be.


\subsubsection{Other specific comments}%
\label{ssub:other_specific_comments}

\begin{description}
  \item[- Figure 55: Would it be possible to move the text plotting on these graphs so the means and yields can be read?]

    Re-arrange the legends for Fig~55--63 such that the important information is clear.

  \item[- Section 6.2: Would it be possible to explicitly define how the upper limit fits are performed?
    Do I understand correctly that there are two fits to the 2012 data set, 
    each with the yield replaced by R(X)/alpha(X)? 
    Why is alpha called a nuisance parameter, 
    isn’t it just a constant whose value is tabulated in Table 38 and 39 for the various configurations?]

    The details of the upper limits calculation are explained in Section 5.7.
    We mainly presents results in Section 6.2.
    For 2012 data, as well as 2016--2018 data, we did perform two fits with yields replaced by R(X)/alpha(X). 
    We do take the alpha as a constant, and the uncertainty of alpha is treated by 
    convolving the normalised likelihood with resolution function.

  \item[Are the fits to Run 2 data simultaneous for each year?
    If so it would be good to add this to the text, 
    along with a description of which variables are shared between the difference categories. 
    Figures 73 and 73 would benefit from being a bit larger.]

    We do perform a simultaneous fit to the 2016--2018 data. 
    The details of the fit are described in Section 5.7.
    Figures 72 and 73 have been zoomed in.

  \item[- Line 1058: You use the same systematics for 2017 and 2018 as 2016 
    using the same for 2016 because the tracking was similar between the years. 
    Is there anyway to quantify how well this assumption holds? 
    Also from Table 34 it looks like the L0 efficiency is your largest systematic, 
    is there a similar way to justify this not changing much between
    the years?]

    The 2016--2018 trigger configurations are simulated in the same
    way, so we expect and the agreement between MC/data on L0 should
    be very similar. 
    As a conservative estimation, 
    we take the biggest systematics (2012 systematics) to calculate the 2016--2018 UL,
    and compare it with the nominal (2016 systematics) result, the difference is about 2\%,
    as you can see in Fig.~\ref{fig:ul}.

    \begin{figure}[tb]
      \centering
      \includegraphics[width=0.5\linewidth]{figs/ul_syscompare_rxicc.pdf}
      \caption{Comparison of the 2016--2018 UL of $R(\Xiccpp)$ with large and nominal systematic uncertainties.}
      \label{fig:ul}
    \end{figure}


  \item[From Figure 54 we can see that the effect of the systematics on the likelihood distribution is small. 
    It might be nice to include one of your upper limit plots with and without systematics included 
    so the reader has an idea of how much this affects the limit.] 

    This is shown in Fig.~69 and 74 in the updated note. The effect becomes larger when the systematics are large.

  \item[- Table 47: I was wondering how the total efficiencies are calculated from the individual efficiencies? 
    Is it just the product as defined in equation 11? 
    If I multiply the columns in Table 47 I don’t end up with the same number as in the total row. 
    The effect is largest for 2015. ]

    Thanks for spotting this. There are two factors here: 
    1) we typeset this table after some auto-print, 
       which causes the wrong number of two cells ``Sel'' and ``PID'' 2015.
       This typo is limited to the display of this table and dose not affect the upper limit results.
    2) we do not include MC-Match efficiency in Table~26 in note v1r1 as a raw here,
    because it is supposed to be divided by instead of multiplied with other components.
    But the ``Total'' efficiency takes into account the MC-Match correction.
    This is explained now in the caption.

  \item[- Equation 3, cross-section in denominator should be the ++ state. ]

    Done.

  \item[- Figure 70: extra curly brace in the x-axis caption]

    Done.

\end{description}
